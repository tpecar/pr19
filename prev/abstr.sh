clear &&
java -cp ./bin/ compiler.Main --target-phase=abstr --logged-phase=abstr --xsl=../../data/ ./prgs/abstr/$1.prev
chromium --allow-file-access-from-files ./prgs/abstr/$1.abstr.xml &>/dev/null &