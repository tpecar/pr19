clear &&
java -cp ./bin/ compiler.Main --target-phase=lexan --logged-phase=lexan --xsl=../../data/ ./prgs/lexan/$1.prev
chromium --allow-file-access-from-files ./prgs/lexan/$1.lexan.xml &>/dev/null &