clear &&
java -cp ./bin/ compiler.Main --target-phase=synan --logged-phase=synan --xsl=../../data/ ./prgs/synan/$1.prev
chromium --allow-file-access-from-files ./prgs/synan/$1.synan.xml &>/dev/null &