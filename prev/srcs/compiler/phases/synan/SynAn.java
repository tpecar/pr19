/**
 * @author sliva
 */
package compiler.phases.synan;

import static compiler.data.dertree.DerNode.Nont.*;
import static compiler.data.symbol.Symbol.Term.*;

import java.util.Vector;
import java.util.function.Function;

import compiler.common.report.*;
import compiler.data.abstree.*;
import compiler.data.dertree.*;
import compiler.data.symbol.Symbol;
import compiler.data.symbol.Symbol.Term;
import compiler.phases.Phase;
import compiler.phases.lexan.LexAn;

/**
 * Syntax analysis.
 * 
 * @author sliva
 */
public class SynAn extends Phase {
	// To enable debugging output
	private final boolean debug = false;


	/** The derivation tree of the program being compiled. */
	public static DerTree derTree = null;

	/** The lexical analyzer used by this syntax analyzer. */
	private final LexAn lexAn;

	/**
	 * Constructs a new phase of syntax analysis.
	 */
	public SynAn() {
		super("synan");
		lexAn = new LexAn();
	}

	@Override
	public void close() {
		lexAn.close();
		super.close();
	}

	/** The lookahead buffer (of length 1).
	 *  Represents the current (unconsumed) terminal on input.
	 */
	private Symbol in = null;

	/**
	 * The parser.
	 * 
	 * This method constructs a derivation tree of the program in the source file.
	 * It calls method {@link #parseSource()} that starts a recursive descent parser
	 * implementation of an LL(1) parsing algorithm.
	 */
	public void parser() {
		in = lexAn.lexer();
		derTree = Source();
		if (in.token != EOF)
			throw new Report.Error(in, "Unexpected '" + in + "' at the end of a program.");
	}

	// ------------------------------------------------------------------------

	/** If we are currently expanding a nullable node */
	boolean nullable = false;

	/**
	 * Sets nullable.
	 * Void - meant to be used before the nret call, to signify that epsilon
	 * expansion is possible.
	 */
	void nullable() {
		nullable = true;
	}

	// debug
	final int debug_spaces = 4;
	int debug_depth = 0;

	String debug_prefix() {
		StringBuilder s = new StringBuilder();
		s.append(String.format("[%3d] ", debug_depth));
		for(int i = 0; i < debug_depth; i++)
			s.append(String.format(".%"+(debug_spaces-1)+"s", ""));
		return s.toString();
	}
	String debug_end_production() {
		StringBuilder s = new StringBuilder();
		s.append("`");
		for(int i = 0; i < debug_spaces-2; i++)
			s.append("-");
		return s.toString();
	}

	/**
	 * Helper class for testing of input & construction of the derivation tree.
	 * 
	 * Methods return true for "should return", for which we can (ab)use the
	 * short circuit evaluation of the logical OR.
	 */
	private class N extends DerNode {

		N(Nont label) {
			super(label);

			if(debug)
			System.out.format(
				"%n%s%s%s :", debug_prefix(), this.label.name(), (nullable ? " N" : "")
			);

			debug_depth++;
		}

		/**
		 * Expect provided first terminal & add to node. Otherwise, return true
		 * (for "should skip"). 
		 * We don't throw an error since we could have multiple productions.
		 */
		boolean ft(Term expectedToken) {

			if(debug)
			System.out.format(
				"%n%s%s [%s]%s ",
				debug_prefix(),
				expectedToken.name(), in.token.name(),
				(in.token == expectedToken ? "*" : "x")
			);

			if (in == null)
			throw new Report.InternalError();
			if (in.token == expectedToken) {
				// We've matched a terminal, so we've chosen the non-nullable
				// production of the nullable nonterminal - from here on
				// the whole production should match
				nullable = false;

				// Input consists of terminals which are always in leaves of the
				// derivation tree
				add(new DerLeaf(in)); 
				in = lexAn.lexer();

				// Continue with this production
				return false;
			}
			else
				// Skip this production
				return true;
		}

		/**
		 * Expect provided non-first terminal & add to node. 
		 * - otherwise throw error
		 */
		boolean t(Term expectedToken) {

			if(debug)
			System.out.format(
				"%s [%s]%s ",
				expectedToken.name(), in.token.name(),
				(in.token == expectedToken ? "*" : "x")
			);

			if (in == null)
			throw new Report.InternalError();
			if (in.token == expectedToken) {
				nullable = false;
				add(new DerLeaf(in));
				in = lexAn.lexer();
				return false;
			}
			else {
				StringBuilder s = new StringBuilder();
				s.append(String.format("Nonterminal %s --> Matched: ", this.label.name()));
				for (DerTree subt : super.subtrees())
					s.append(
						subt instanceof DerNode ?
							String.format("Nont %s (%s) ", ((DerNode)subt).label.name(), subt.location()) :
							String.format("Term %s (%s) ", ((DerLeaf)subt).symb.token.name(), subt.location())
					);
				s.append(String.format(
					", Next expected: %s, got %s",
					expectedToken.name(), in.token.name()
				));
				throw new Report.Error(in, s.toString());
			}
		}

		/** 
		 * Expect provided nonterminal subtree & add to node. Otherwise
		 * (if nonterminal expansion method returned DerNullable)
		 *  - if we are in nullable nonterminal expansion, return true
		 *    (for "should return") - this way we skip all other expansions
		 *    up until the nullable nonterminal
		 *  - otherwise throw error
		 */
		boolean n(DerTree subtree) {
			if (!subtree.produced()) {
				if(nullable)
					return true;
				// shouldn't get null for a non-nullable nonterminal
				else throw new Report.InternalError();
			}

			add(subtree);
			return false;
		}

		/**
		 * Set the AST conversion function & return false (continue).
		 * 
		 * The lambda
		 *  argument (AbsTree) represents the left subtree to be appended
		 *                     to this nodes AST
		 *  result (AbsTree) represents the AST of this node
		 */
		boolean ast(Function<AbsTree, AbsTree> toAST) {
			// Assigning a null function indicates an error
			if(toAST == null)
				throw new Report.InternalError();
			
			// Multiple assignment of conversion function indicates an error
			if(super.toAST != null)
				throw new Report.InternalError();
			
			super.toAST = toAST;
			return false;
		}

		/**
		 * Accepts boolean, returns itself.
		 * If return value is true (skip otherwise non-nullable nonterminal)
		 *		nullable is true, return DerNulled
		 *		nullable is false, throw error.
		 * If return value is false, normal expansion, return itself.
		 */
		DerNode ret(boolean retval) {
			debug_depth--;

			if(debug)
			System.out.format(
				"%n%s%s %s %s%s%n%s",
				debug_prefix(), debug_end_production(),
				(retval ? "skip" : "end"),
				(nullable ? "N " : ""),
				super.label.name(),
				debug_prefix()
			);

			if(retval)
				if (nullable)
					return new DerNulled();
				else
					throw new Report.Error(in, String.format(
						"Nonterminal %s -/-> input %s",
						this.label.name(), in.token.name()
					));
			return this;
		}

		/**
		 * Accepts boolean, returns itself, sets nullable to false
		 */
		DerNode nret(boolean retval) {
			debug_depth--;

			if(debug)
			System.out.format(
				"%n%s%s %s %s%n%s",
				debug_prefix(), debug_end_production(),
				(retval ? "nulled" : "end"),
				super.label.name(),
				debug_prefix()
			);

			nullable = false;
			return this;
		}

		// ---------------------------------------------------------------------
		// This is to minimize the boilerplate when accessing subtree members.

		/**
		 * Returns the DerTree (either DerNode or DerLeaf) of the subtree of this node.
		 */
		DerTree sub(int subIndex) {
			return ((DerNode)this).subtree(subIndex);
		}

		DerLeaf subLeaf(int subIndex) {
			return (DerLeaf)this.sub(subIndex);
		}
	}

	// ------------------------------------------------------------------------
	// Recursive descent parser methods
	// ------------------------------------------------------------------------
	// These methods represent the productions of our grammar:
	//	- the call of the methods represents the selection of a nonterminal
	//    (and determines how the following sequence is getting interpreted)
	//  - the Symbol in represents the current unconsumed terminal that either
	//		- selects which production we will choose
	//        (if we are at the first terminal of a production)
	//		- should match what the current production expects
	//        (if we are at the non-first terminal of the selected production)
	// ------------------------------------------------------------------------
	//
	// We will provide the productions for nonterminals in the order described
	// in the DerNode.Nont enum
	//

	// Source		--------------------------------------------------------- //
	//
	// Source --> Decl Decls
	//
	/**
	 * Source nonterminal production expansion.
	 *
	 * This essentially starts the recursive descent parsing algorithm
	 * (which complies to the rules of LL(1) parsing).
	 */
	private DerNode Source() {
		N n = new N(Source);
		
		// Source node has only one subtree, and that subtree is essentially
		// the root of the derivation tree.
		//
		return n.ret(
			(n.n(Decl()) || n.n(Decls()) || n.t(EOF) || n.ast
			((no_lsub) -> {
				Vector<AbsDecl> decls = new Vector<>();

				// Decl
				decls.add((AbsDecl)n.sub(0).toAST(null));

				// Decls
				AbsTree declsRest = n.sub(1).toAST(null);
				if(declsRest.produced()) decls.addAll(((AbsDecls)declsRest).decls());

				AbsDecls absDecls = new AbsDecls(
					new Location(decls.firstElement(), decls.lastElement()),
					decls
				);

				return new AbsSource(n, absDecls);
			}))
		);
	}

	// Decls		--------------------------------------------------------- //
	//
	// Decls --> Decl Decls | eps
	//
	private DerNode Decls() {
		N n = new N(Decls);

		nullable();
		return n.nret(
			(n.n(Decl()) || n.n(Decls()) || n.ast
			((no_lsub) -> {
				// The toAST call will check if the node was nulled & 
				// will return null, before we get to this functionality

				Vector<AbsDecl> decls = new Vector<>();

				// Decl
				decls.add((AbsDecl)n.sub(0).toAST(null));

				// Decls
				AbsTree declsRest = n.sub(1).toAST(null);
				if(declsRest.produced()) decls.addAll(((AbsDecls)declsRest).decls());

				return new AbsDecls(
					new Location(decls.firstElement(), decls.lastElement()),
					decls
				);
			}))
		);
	}

	// Decl			--------------------------------------------------------- //
	//
	// Decl --> TYP IDENTIFIER : Type ; 
	// Decl --> VAR IDENTIFIER : Type ; 
	// Decl --> FUN IDENTIFIER \( ParDeclsEps \) : Type BodyEps; 
	//
	private DerNode Decl() {
		N n = new N(Decl);

		return n.ret(
			// Combine productions using && - this way, if we skipped a
			// production by returning true, we try next one.
			//
			// Otherwise we return.
			//
			(n.ft(TYP) || n.t(IDENTIFIER) || n.t(COLON) || n.n(Type()) || n.t(SEMIC) || n.ast
			(no_lsub -> {
				String name = n.subLeaf(1).symb.lexeme;
				AbsType absType = (AbsType)n.sub(3).toAST(null);

				return new AbsTypDecl(n, name, absType);
			})) &&

			(n.ft(VAR) || n.t(IDENTIFIER) || n.t(COLON) || n.n(Type()) || n.t(SEMIC) || n.ast
			(no_lsub -> {
				String name = n.subLeaf(1).symb.lexeme;
				AbsType absType = (AbsType)n.sub(3).toAST(null);

				return new AbsVarDecl(n, name, absType);
			})) &&

			(n.ft(FUN) || n.t(IDENTIFIER) || n.t(LPARENTHESIS) || n.n(ParDeclsEps()) ||
				n.t(RPARENTHESIS) || n.t(COLON) || n.n(Type()) || n.n(BodyEps()) || n.t(SEMIC) || n.ast
			(no_lsub -> {
				String name = n.subLeaf(1).symb.lexeme;

				AbsTree parDeclsEps = n.sub(3).toAST(null);
				AbsParDecls parDecls = 
					parDeclsEps.produced() ?
					(AbsParDecls)parDeclsEps :
					new AbsParDecls(parDeclsEps, new Vector<AbsParDecl>());
				
				AbsType type = (AbsType)n.sub(6).toAST(null);
				AbsTree value = n.sub(7).toAST(null);
				
				if(value.produced())
					// body, full function definition
					return new AbsFunDef(n, name, parDecls, type, (AbsExpr)value);
				else
					// no body, so only declaration
					return new AbsFunDecl(n, name, parDecls, type);
			}))
		);
	}

	// ParDeclsEps	--------------------------------------------------------- //
	//
	// ParDeclsEps --> IDENTIFIER : Type ParDecls | eps
	//
	private DerNode ParDeclsEps() {
		N n = new N(ParDeclsEps);
		
		nullable();
		return n.nret(
			(n.ft(IDENTIFIER) || n.t(COLON) || n.n(Type()) || n.n(ParDecls()) || n.ast
			(no_lsub -> {
				String name = n.subLeaf(0).symb.lexeme;
				AbsType type = (AbsType)n.sub(2).toAST(null);

				Vector<AbsParDecl> parDecls = new Vector<>();

				parDecls.add(new AbsParDecl(n.sub(2), name, type));

				AbsTree parDeclsRest = n.sub(3).toAST(null);
				if(parDeclsRest.produced())
					parDecls.addAll(((AbsParDecls)parDeclsRest).parDecls());
				
				return new AbsParDecls(n, parDecls);
			}))
		);
	}

	// ParDecls		--------------------------------------------------------- //
	//
	// ParDecls --> , IDENTIFIER : Type ParDecls | eps
	//
	private DerNode ParDecls() {
		N n = new N(ParDecls);

		nullable();
		return n.nret(
			(n.ft(COMMA) || n.t(IDENTIFIER) || n.t(COLON) ||
			n.n(Type()) || n.n(ParDecls()) || n.ast
			(no_lsub -> {
				String name = n.subLeaf(1).symb.lexeme;
				AbsType type = (AbsType)n.sub(3).toAST(null);

				Vector<AbsParDecl> parDecls = new Vector<>();

				parDecls.add(new AbsParDecl(n.sub(3), name, type));
				AbsTree parDeclsRest = n.sub(4).toAST(null);
				if(parDeclsRest.produced())
					parDecls.addAll(((AbsParDecls)parDeclsRest).parDecls());
				
				return new AbsParDecls(n, parDecls);
			}))
		);
	}

	// BodyEps		--------------------------------------------------------- //
	//
	// BodyEps --> = Expr | eps
	//
	private DerNode BodyEps() {
		N n = new N(BodyEps);

		nullable();
		return n.nret(
			(n.ft(ASSIGN) || n.n(Expr()) || n.ast
			(no_lsub -> {
				// The underlying expression might be many different derivations -
				// the only important thing here is that they get derived into an
				// AbsExpr castable type
				return (AbsExpr)n.sub(1).toAST(null);
			}))
		);
	}

	// Type			--------------------------------------------------------- //
	//
	// Type --> VOID | BOOL | CHAR | INT 
	// Type --> ARR \[Expr\] Type 
	// Type --> REC \(IDENTIFIER : Type ParDecls\) 
	// Type --> PTR Type 
	// Type --> IDENTIFIER 
	// Type --> \( Type \) 
	//
	private DerNode Type() {
		N n = new N(Type);

		return n.ret(
			(n.ft(VOID) || n.ast
			(no_lsub -> new AbsAtomType(n, AbsAtomType.Type.VOID))) &&

			(n.ft(BOOL) || n.ast
			(no_lsub -> new AbsAtomType(n, AbsAtomType.Type.BOOL))) &&

			(n.ft(CHAR) || n.ast
			(no_lsub -> new AbsAtomType(n, AbsAtomType.Type.CHAR))) &&

			(n.ft(INT) || n.ast
			(no_lsub -> new AbsAtomType(n, AbsAtomType.Type.INT))) &&

			(n.ft(ARR) || n.t(LBRACKET) || n.n(Expr()) || n.t(RBRACKET) || n.n(Type()) || n.ast
			(no_lsub -> {
				AbsExpr len = (AbsExpr)n.sub(2).toAST(null);
				AbsType elemType = (AbsType)n.sub(4).toAST(null);

				return new AbsArrType(n, len, elemType);
			})) &&

			(n.ft(REC) || n.t(LPARENTHESIS) || n.t(IDENTIFIER) || n.t(COLON) ||
			 n.n(Type()) || n.n(CompDecls()) || n.t(RPARENTHESIS) || n.ast
			(no_lsub -> {
				String name = n.subLeaf(2).symb.lexeme;
				AbsType type = (AbsType)n.sub(4).toAST(null);

				Vector<AbsCompDecl> compDecls = new Vector<>();
				compDecls.add(new AbsCompDecl(
					new Location(n.sub(2), n.sub(4)), name, type)
				);
				AbsTree compDeclsRest = n.sub(5).toAST(null);
				if(compDeclsRest.produced())
					compDecls.addAll(((AbsCompDecls)compDeclsRest).compDecls());

				AbsCompDecls recCompDecls = new AbsCompDecls(
					new Location(compDecls.firstElement(), compDecls.lastElement()),
					compDecls
				);
				return new AbsRecType(n, recCompDecls);
			})) &&

			(n.ft(PTR) || n.n(Type()) || n.ast
			(no_lsub -> {
				AbsType subType = (AbsType)n.sub(1).toAST(null);

				return new AbsPtrType(n, subType);
			})) &&

			(n.ft(IDENTIFIER) || n.ast
			(no_lsub -> {
				return new AbsTypName(n, n.subLeaf(0).symb.lexeme);
			})) &&

			(n.ft(LPARENTHESIS) || n.n(Type()) || n.t(RPARENTHESIS) || n.ast
			(no_lsub -> {
				// Return AST from enclosed expression
				// Only inner expr in AST
				return n.sub(1).toAST(null);
			}))
		);
	}

	// CompDecls			--------------------------------------------------------- //
	//
	// CompDecls --> , IDENTIFIER : Type | eps
	//

	private DerNode CompDecls() {
		N n = new N(CompDecls);

		nullable();
		return n.nret(
			(n.ft(COMMA) || n.t(IDENTIFIER) || n.t(COLON) || n.n(Type()) || n.n(CompDecls()) ||
			n.ast
			(no_lsub -> {
				String name = n.subLeaf(1).symb.lexeme;
				AbsType type = (AbsType)n.sub(3).toAST(null);

				Vector<AbsCompDecl> compDecls = new Vector<>();
				compDecls.add(new AbsCompDecl(
					new Location(n.sub(1), n.sub(3)), name, type)
				);
				AbsTree compDeclsRest = n.sub(4).toAST(null);
				if(compDeclsRest.produced())
					compDecls.addAll(((AbsCompDecls)compDeclsRest).compDecls());

				return new AbsCompDecls(
					new Location(compDecls.firstElement(), compDecls.lastElement()),
					compDecls
				);
			}))
		);
	}

	// Expr			--------------------------------------------------------- //
	//
	// Expr --> DisjExpr
	//
	private DerNode Expr() {
		N n = new N(Expr);

		return n.ret(
			(n.n(DisjExpr()) || n.ast
			(no_lsub -> {
				// Only inner expr in AST
				return n.sub(0).toAST(null);
			}))
		);
	}

	// DisjExpr		--------------------------------------------------------- //
	//
	// DisjExpr --> ConjExpr DisjExprRest
	//
	private DerNode DisjExpr() {
		N n = new N(DisjExpr);

		return n.ret(
			(n.n(ConjExpr()) || n.n(DisjExprRest()) || n.ast
			(no_lsub -> {
				// Binary operator, we provide the left subtree as the argument to
				// to the right subtree AST conversion & return its result back up
				//
				// We're enforcing explicit casts here so that possible type issues
				// will be discovered ASAP
				AbsExpr lsub = (AbsExpr)n.sub(0).toAST(null);

				// Also, in the case where xRest AST conversion can return null
				// (due to not being produced), we return the lsub
				AbsTree disjExprRest = n.sub(1).toAST(lsub);
				return disjExprRest.produced() ? (AbsExpr)disjExprRest : lsub;
			}))
		);
	}

	// DisjExprRest	--------------------------------------------------------- //
	//
	// DisjExprRest --> IOR ConjExpr DisjExprRest | XOR ConjExpr DisjExprRest | eps
	//
	private DerNode DisjExprRest() {
		N n = new N(DisjExprRest);

		nullable();
		return n.nret(
			(n.ft(IOR) || n.n(ConjExpr()) || n.n(DisjExprRest()) || n.ast
			(lsub -> {
				// We are provided with the left subtree here
				// lsub and ConjExpr make up the AbsBinExpr
				AbsExpr conjExpr = (AbsExpr)n.sub(1).toAST(null);
				AbsBinExpr binExpr = new AbsBinExpr(
					new Location(lsub, conjExpr),
					AbsBinExpr.Oper.IOR,
					(AbsExpr)lsub, conjExpr
				);

				// In case we have a non-nulled DisjExprRest AST here, we will
				// provide the current AbsBinExpr as the left subtree to
				// the DisjExprRest AST conversion
				//
				// The resulting AST will be left hanging, and since we do
				// left-deep first recursive traversal during evaluation, we
				// preserve the left associativity of operators this way.
				//
				AbsTree disjExprRest = n.sub(2).toAST(binExpr);

				// If the DisjExprRest was nulled, we return back binExpr directly
				return disjExprRest.produced() ? (AbsExpr)disjExprRest : binExpr;
			})) &&

			(n.ft(XOR) || n.n(ConjExpr()) || n.n(DisjExprRest()) || n.ast
			(lsub -> {
				AbsExpr conjExpr = (AbsExpr)n.sub(1).toAST(null);
				AbsBinExpr binExpr = new AbsBinExpr(
					new Location(lsub, conjExpr),
					AbsBinExpr.Oper.XOR,
					(AbsExpr)lsub, conjExpr
				);

				AbsTree disjExprRest = n.sub(2).toAST(binExpr);
				return disjExprRest.produced() ? (AbsExpr)disjExprRest : binExpr;
			}))
		);
	}

	// ConjExpr		--------------------------------------------------------- //
	//
	// ConjExpr --> RelExpr ConjExprRest
	//
	private DerNode ConjExpr() {
		N n = new N(ConjExpr);

		return n.ret(
			(n.n(RelExpr()) || n.n(ConjExprRest()) || n.ast
			(no_lsub -> {
				AbsExpr lsub = (AbsExpr)n.sub(0).toAST(null);
				AbsTree conjExprRest = n.sub(1).toAST(lsub);
				return conjExprRest.produced() ? (AbsExpr)conjExprRest : lsub;
			}))
		);
	}

	// ConjExprRest	--------------------------------------------------------- //
	//
	// ConjExprRest --> AND RelExpr ConjExprRest | eps
	//
	private DerNode ConjExprRest() {
		N n = new N(ConjExprRest);

		nullable();
		return n.nret(
			(n.ft(AND) || n.n(RelExpr()) || n.n(ConjExprRest()) || n.ast
			(lsub -> {
				AbsExpr relExpr = (AbsExpr)n.sub(1).toAST(null);
				AbsBinExpr binExpr = new AbsBinExpr(
					new Location(lsub, relExpr),
					AbsBinExpr.Oper.AND,
					(AbsExpr)lsub, relExpr
				);
				AbsTree conjExprRest = n.sub(2).toAST(binExpr);
				return conjExprRest.produced() ? (AbsExpr)conjExprRest : binExpr;
			}))
		);
	}

	// RelExpr		--------------------------------------------------------- //
	//
	// RelExpr --> AddExpr RelExprRest
	//
	private DerNode RelExpr() {
		N n = new N(RelExpr);

		return n.ret(
			(n.n(AddExpr()) || n.n(RelExprRest()) || n.ast
			(no_lsub -> {
				AbsExpr lsub = (AbsExpr)n.sub(0).toAST(null);
				AbsTree relExprRest = n.sub(1).toAST(lsub);
				return relExprRest.produced() ? (AbsExpr)relExprRest : lsub;
			}))
		);
	}

	// RelExprRest	--------------------------------------------------------- //
	//
	// RelExprRest --> EQU AddExpr | NEQ AddExpr | LEQ AddExpr | GEQ AddExpr |
	//                 LTH AddExpr | GTH AddExpr | eps
	//
	private DerNode RelExprRest() {
		N n = new N(RelExprRest);

		nullable();
		return n.nret(
			(n.ft(EQU) || n.n(AddExpr()) || n.ast
			(lsub -> {
				// Relational expressions aren't associative (can't be used
				// directly one after another), so we simply return the binop
				// back here
				AbsExpr addExpr = (AbsExpr)n.sub(1).toAST(null);
				return new AbsBinExpr(
					new Location(lsub, addExpr),
					AbsBinExpr.Oper.EQU,
					(AbsExpr)lsub, addExpr
				);
			})) &&

			(n.ft(NEQ) || n.n(AddExpr()) || n.ast
			(lsub -> {
				AbsExpr addExpr = (AbsExpr)n.sub(1).toAST(null);
				return new AbsBinExpr(
					new Location(lsub, addExpr),
					AbsBinExpr.Oper.NEQ,
					(AbsExpr)lsub, addExpr
				);
			})) &&

			(n.ft(LEQ) || n.n(AddExpr()) || n.ast
			(lsub -> {
				AbsExpr addExpr = (AbsExpr)n.sub(1).toAST(null);
				return new AbsBinExpr(
					new Location(lsub, addExpr),
					AbsBinExpr.Oper.LEQ,
					(AbsExpr)lsub, addExpr
				);
			})) &&

			(n.ft(GEQ) || n.n(AddExpr()) || n.ast
			(lsub -> {
				AbsExpr addExpr = (AbsExpr)n.sub(1).toAST(null);
				return new AbsBinExpr(
					new Location(lsub, addExpr),
					AbsBinExpr.Oper.GEQ,
					(AbsExpr)lsub, addExpr
				);
			})) &&

			(n.ft(LTH) || n.n(AddExpr()) || n.ast
			(lsub -> {
				AbsExpr addExpr = (AbsExpr)n.sub(1).toAST(null);
				return new AbsBinExpr(
					new Location(lsub, addExpr),
					AbsBinExpr.Oper.LTH,
					(AbsExpr)lsub, addExpr
				);
			})) &&

			(n.ft(GTH) || n.n(AddExpr()) || n.ast
			(lsub -> {
				AbsExpr addExpr = (AbsExpr)n.sub(1).toAST(null);
				return new AbsBinExpr(
					new Location(lsub, addExpr),
					AbsBinExpr.Oper.GTH,
					(AbsExpr)lsub, addExpr
				);
			}))
		);
	}

	// AddExpr		--------------------------------------------------------- //
	//
	// AddExpr --> MulExpr AddExprRest
	//
	private DerNode AddExpr() {
		N n = new N(AddExpr);

		return n.ret(
			(n.n(MulExpr()) || n.n(AddExprRest()) || n.ast
			(no_lsub -> {
				AbsExpr lsub = (AbsExpr)n.sub(0).toAST(null);
				AbsTree addExprRest = n.sub(1).toAST(lsub);
				return addExprRest.produced() ? (AbsExpr)addExprRest : lsub;
			}))
		);
	}
	
	// AddExprRest	--------------------------------------------------------- //
	//
	// AddExprRest --> ADD MulExpr AddExprRest | SUB MulExpr AddExprRest | eps
	//
	private DerNode AddExprRest() {
		N n = new N(AddExprRest);

		nullable();
		return n.nret(
			(n.ft(ADD) || n.n(MulExpr()) || n.n(AddExprRest()) || n.ast
			(lsub -> {
				AbsExpr mulExpr = (AbsExpr)n.sub(1).toAST(null);
				AbsBinExpr binExpr = new AbsBinExpr(
					new Location(lsub, mulExpr),
					AbsBinExpr.Oper.ADD,
					(AbsExpr)lsub, mulExpr
				);

				AbsTree addExprRest = n.sub(2).toAST(binExpr);
				return addExprRest.produced() ? (AbsExpr)addExprRest : binExpr;
			})) &&

			(n.ft(SUB) || n.n(MulExpr()) || n.n(AddExprRest()) || n.ast
			(lsub -> {
				AbsExpr mulExpr = (AbsExpr)n.sub(1).toAST(null);
				AbsBinExpr binExpr = new AbsBinExpr(
					new Location(lsub, mulExpr),
					AbsBinExpr.Oper.SUB,
					(AbsExpr)lsub, mulExpr
				);

				AbsTree addExprRest = n.sub(2).toAST(binExpr);
				return addExprRest.produced() ? (AbsExpr)addExprRest : binExpr;
			}))
		);
	}

	// MulExpr		--------------------------------------------------------- //
	//
	// MulExpr --> PrefExpr MulExprRest
	//
	private DerNode MulExpr() {
		N n = new N(MulExpr);

		return n.ret(
			(n.n(PrefExpr()) || n.n(MulExprRest()) || n.ast
			(no_lsub -> {
				AbsExpr lsub = (AbsExpr)n.sub(0).toAST(null);
				AbsTree mulExprRest = n.sub(1).toAST(lsub);
				return mulExprRest.produced() ? (AbsExpr)mulExprRest : lsub;
			}))
		);
	}

	// MulExprRest	--------------------------------------------------------- //
	//
	// MulExprRest --> MUL PrefExpr MulExprRest | DIV PrefExpr MulExprRest |
	//                 MOD PrefExpr MulExprRest | eps
	//
	private DerNode MulExprRest() {
		N n = new N(MulExprRest);

		nullable();
		return n.nret(
			(n.ft(MUL) || n.n(PrefExpr()) || n.n(MulExprRest()) || n.ast
			(lsub -> {
				AbsExpr prefExpr = (AbsExpr)n.sub(1).toAST(null);
				AbsBinExpr binExpr = new AbsBinExpr(
					new Location(lsub, prefExpr),
					AbsBinExpr.Oper.MUL,
					(AbsExpr)lsub, prefExpr
				);

				AbsTree mulExprRest = n.sub(2).toAST(binExpr);
				return mulExprRest.produced() ? (AbsExpr)mulExprRest : binExpr;
			})) &&

			(n.ft(DIV) || n.n(PrefExpr()) || n.n(MulExprRest()) || n.ast
			(lsub -> {
				AbsExpr prefExpr = (AbsExpr)n.sub(1).toAST(null);
				AbsBinExpr binExpr = new AbsBinExpr(
					new Location(lsub, prefExpr),
					AbsBinExpr.Oper.DIV,
					(AbsExpr)lsub, prefExpr
				);

				AbsTree mulExprRest = n.sub(2).toAST(binExpr);
				return mulExprRest.produced() ? (AbsExpr)mulExprRest : binExpr;
			})) &&

			(n.ft(MOD) || n.n(PrefExpr()) || n.n(MulExprRest()) || n.ast
			(lsub -> {
				AbsExpr prefExpr = (AbsExpr)n.sub(1).toAST(null);
				AbsBinExpr binExpr = new AbsBinExpr(
					new Location(lsub, prefExpr),
					AbsBinExpr.Oper.MOD,
					(AbsExpr)lsub, prefExpr
				);

				AbsTree mulExprRest = n.sub(2).toAST(binExpr);
				return mulExprRest.produced() ? (AbsExpr)mulExprRest : binExpr;
			}))
		);
	}

	// PrefExpr		--------------------------------------------------------- //
	//
	// PrefExpr -->
	//		NOT PrefExpr | ADD PrefExpr | SUB PrefExpr | DATA PrefExpr |
	//		ADDR PrefExpr |
	//		NEW \(Type\) | DEL \(Expr\) |
	//		AtomExpr
	// 
	private DerNode PrefExpr() {
		N n = new N(PrefExpr);

		return n.ret(
			(n.ft(NOT) || n.n(PrefExpr()) || n.ast
			(no_lsub -> {
				AbsExpr atomExpr = (AbsExpr)n.sub(1).toAST(null);
				return new AbsUnExpr(n, AbsUnExpr.Oper.NOT, atomExpr);
			})) &&

			(n.ft(ADD) || n.n(PrefExpr()) || n.ast
			(no_lsub -> {
				AbsExpr atomExpr = (AbsExpr)n.sub(1).toAST(null);
				return new AbsUnExpr(n, AbsUnExpr.Oper.ADD, atomExpr);
			})) &&

			(n.ft(SUB) || n.n(PrefExpr()) || n.ast
			(no_lsub -> {
				AbsExpr atomExpr = (AbsExpr)n.sub(1).toAST(null);
				return new AbsUnExpr(n, AbsUnExpr.Oper.SUB, atomExpr);
			})) &&
			
			(n.ft(DATA) || n.n(PrefExpr()) || n.ast
			(no_lsub -> {
				AbsExpr atomExpr = (AbsExpr)n.sub(1).toAST(null);
				return new AbsUnExpr(n, AbsUnExpr.Oper.DATA, atomExpr);
			})) &&

			(n.ft(ADDR) || n.n(PrefExpr()) || n.ast
			(no_lsub -> {
				AbsExpr atomExpr = (AbsExpr)n.sub(1).toAST(null);
				return new AbsUnExpr(n, AbsUnExpr.Oper.ADDR, atomExpr);
			})) &&

			(n.ft(NEW) || n.t(LPARENTHESIS) || n.n(Type()) || n.t(RPARENTHESIS) || n.ast
			(no_lsub -> {
				AbsType type = (AbsType)n.sub(2).toAST(null);
				return new AbsNewExpr(n, type);
			})) &&

			(n.ft(DEL) || n.t(LPARENTHESIS) || n.n(Expr()) || n.t(RPARENTHESIS) || n.ast
			(no_lsub -> {
				AbsExpr expr = (AbsExpr)n.sub(2).toAST(null);
				return new AbsDelExpr(n, expr);
			})) &&


			(n.n(AtomExpr()) || n.ast
			(no_lsub -> {
				return n.sub(0).toAST(null);
			}))
		);
	}

	// PstfExpr		--------------------------------------------------------- //
	//
	// PstfExpr --> \[ Expr \] PstfExpr | . IDENTIFIER PstfExpr | eps
	//
	private DerNode PstfExpr() {
		N n = new N(PstfExpr);

		nullable();
		return n.nret(
			// The postfix expression productions produce the final
			// AbsRecExpr / AbsArrExpr containing the expression they postfix,
			// so we need to provide the left-hand-side as lsub
			//
			(n.ft(LBRACKET) || n.n(Expr()) || n.t(RBRACKET) || n.n(PstfExpr()) || n.ast
			(lsub -> {
				AbsExpr index = (AbsExpr)n.sub(1).toAST(null);
				AbsArrExpr arrExpr = new AbsArrExpr(
					new Location(lsub, index),
					(AbsExpr)lsub,
					index
				);

				// The resulting AbsArrExpr can again be left-hand-side to
				// another PstfExpr
				AbsTree pstfExpr = n.sub(3).toAST(arrExpr);

				return pstfExpr.produced() ? (AbsExpr)pstfExpr : arrExpr;
			})) &&

			(n.ft(DOT) || n.t(IDENTIFIER) || n.n(PstfExpr()) || n.ast
			(lsub -> {
				AbsVarName comp = new AbsVarName(n.sub(1), n.subLeaf(1).symb.lexeme);
				AbsRecExpr record = new AbsRecExpr(
					new Location(lsub, comp),
					(AbsExpr)lsub,
					comp
				);

				AbsTree pstfExpr = n.sub(2).toAST(record);

				return pstfExpr.produced() ? (AbsExpr)pstfExpr : record;
			}))
		);
	}

	// AtomExpr 	--------------------------------------------------------- //
	//
	// AtomExpr --> \( Expr CastEps \) PstfExpr
	// AtomExpr --> IDENTIFIER CallEps PstfExpr
	// AtomExpr -->
	//		VOIDCONST PstfExpr | BOOLCONST PstfExpr | CHARCONST PstfExpr |
	//		INTCONST PstfExpr | PTRCONST  PstfExpr| STRCONST PstfExpr
	// AtomExpr --> \{ Stmt Stmts : Expr WhereEps \} PstfExpr
	//
	private DerNode AtomExpr() {
		N n = new N(AtomExpr);

		return n.ret(
			(n.ft(LPARENTHESIS) || n.n(Expr()) || n.n(CastEps()) || n.ft(RPARENTHESIS) ||
			 n.n(PstfExpr()) || n.ast
			(no_lsub -> {
				// The Expr can be either an enclosed expression or part of cast expression
				//
				AbsExpr expr = (AbsExpr)n.sub(1).toAST(null);
				AbsTree castExpr = n.sub(2).toAST(expr);

				AbsExpr pstfExprLsub = castExpr.produced() ? (AbsExpr)castExpr : expr;

				AbsTree pstfExpr = n.sub(4).toAST(pstfExprLsub);

				return pstfExpr.produced() ? (AbsExpr)pstfExpr : pstfExprLsub;
			})) &&

			(n.ft(IDENTIFIER) || n.n(CallEps()) || n.n(PstfExpr()) || n.ast
			(no_lsub -> {
				// We either have an identifier or function call (if parameters present)
				AbsTree args = n.sub(1).toAST(null);
				AbsVarName varName;

				if(args.produced())
					// Function call
					varName = new AbsFunName(
						new Location(n.sub(0), n.sub(1)),
						n.subLeaf(0).symb.lexeme,
						(AbsArgs)args
					);
				else 
					// Variable
					varName = new AbsVarName(
						n.sub(0),
						n.subLeaf(0).symb.lexeme
					);

				AbsTree pstfExpr = n.sub(2).toAST(varName);
				
				return pstfExpr.produced() ? (AbsExpr)pstfExpr : varName;
			})) &&

			(n.ft(VOIDCONST) || n.n(PstfExpr()) || n.ast
			(no_lsub -> {
				AbsAtomExpr atomExpr = new AbsAtomExpr(
					n.sub(0),
					AbsAtomExpr.Type.VOID,
					n.subLeaf(0).symb.lexeme
				);
				AbsTree pstfExpr = n.sub(1).toAST(atomExpr);
				return pstfExpr.produced() ? (AbsExpr)pstfExpr : atomExpr;
			})) &&

			(n.ft(BOOLCONST) || n.n(PstfExpr()) || n.ast
			(no_lsub -> {
				AbsAtomExpr atomExpr = new AbsAtomExpr(
					n.sub(0),
					AbsAtomExpr.Type.BOOL,
					n.subLeaf(0).symb.lexeme
				);
				AbsTree pstfExpr = n.sub(1).toAST(atomExpr);
				return pstfExpr.produced() ? (AbsExpr)pstfExpr : atomExpr;
			})) &&

			(n.ft(CHARCONST) || n.n(PstfExpr()) || n.ast
			(no_lsub -> {
				AbsAtomExpr atomExpr = new AbsAtomExpr(
					n.sub(0),
					AbsAtomExpr.Type.CHAR,
					n.subLeaf(0).symb.lexeme
				);
				AbsTree pstfExpr = n.sub(1).toAST(atomExpr);
				return pstfExpr.produced() ? (AbsExpr)pstfExpr : atomExpr;
			})) &&

			(n.ft(INTCONST) || n.n(PstfExpr()) || n.ast
			(no_lsub -> {
				AbsAtomExpr atomExpr = new AbsAtomExpr(
					n.sub(0),
					AbsAtomExpr.Type.INT,
					n.subLeaf(0).symb.lexeme
				);
				AbsTree pstfExpr = n.sub(1).toAST(atomExpr);
				return pstfExpr.produced() ? (AbsExpr)pstfExpr : atomExpr;
			})) &&

			(n.ft(PTRCONST) || n.n(PstfExpr()) || n.ast
			(no_lsub -> {
				AbsAtomExpr atomExpr = new AbsAtomExpr(
					n.sub(0),
					AbsAtomExpr.Type.PTR,
					n.subLeaf(0).symb.lexeme
				);
				AbsTree pstfExpr = n.sub(1).toAST(atomExpr);
				return pstfExpr.produced() ? (AbsExpr)pstfExpr : atomExpr;
			})) &&

			(n.ft(STRCONST) || n.n(PstfExpr()) || n.ast
			(no_lsub -> {
				AbsAtomExpr atomExpr = new AbsAtomExpr(
					n.sub(0),
					AbsAtomExpr.Type.STR,
					n.subLeaf(0).symb.lexeme
				);
				AbsTree pstfExpr = n.sub(1).toAST(atomExpr);
				return pstfExpr.produced() ? (AbsExpr)pstfExpr : atomExpr;
			})) &&

			(n.ft(LBRACE) || n.n(Stmt()) || n.n(Stmts()) || n.t(COLON) ||
			n.n(Expr()) || n.n(WhereEps()) || n.t(RBRACE) || n.n(PstfExpr()) || n.ast
			(no_lsub -> {
				Vector<AbsStmt> stmts = new Vector<>();
				stmts.add((AbsStmt)n.sub(1).toAST(null));

				AbsTree stmtsRest = n.sub(2).toAST(null);
				if(stmtsRest.produced())
					stmts.addAll(((AbsStmts)stmtsRest).stmts());

				AbsStmts absStmts = new AbsStmts(
					new Location(stmts.firstElement(), stmts.lastElement()),
					stmts
				);
				
				AbsExpr expr = (AbsExpr)n.sub(4).toAST(null);

				// AbsDecls provided by WhereEps
				AbsTree declsRest = n.sub(5).toAST(null);
				AbsDecls decls = declsRest.produced() ?
					(AbsDecls)declsRest :
					new AbsDecls(new Location(0,0, 0,0), new Vector<AbsDecl>());
				
				AbsBlockExpr blockExpr = new AbsBlockExpr(
					new Location(n.sub(0), n.sub(6)),
					decls, absStmts, expr
				);

				AbsTree pstfExpr = n.sub(7).toAST(blockExpr);

				return pstfExpr.produced() ? (AbsExpr)pstfExpr : blockExpr;
			}))
		);
	}

	// CallEps		--------------------------------------------------------- //
	//
	// CallEps --> \(Args\) | eps
	//
	private DerNode CallEps() {
		N n = new N(CallEps);

		nullable();
		return n.nret(
			(n.ft(LPARENTHESIS) || n.n(Args()) || n.t(RPARENTHESIS) || n.ast
			(no_lsub -> {
				// In the case where the function call has no arguments,
				// (we have LPAR, RPAR, but Args get nulled), we return
				// empty AbsArgs
				AbsTree absArgs = n.sub(1).toAST(null);
				return absArgs.produced() ?
					absArgs :
					new AbsArgs(n, new Vector<AbsExpr>());
			}))
		);
	}

	// Args			--------------------------------------------------------- //
	//
	// Args --> Expr ArgsRest | eps
	//
	private DerNode Args() {
		N n = new N(Args);

		nullable();
		return n.nret(
			(n.n(Expr()) || n.n(ArgsRest()) || n.ast
			(no_lsub -> {
				Vector<AbsExpr> args = new Vector<>();
				args.add((AbsExpr)n.sub(0).toAST(null));
				AbsTree argsRest = n.sub(1).toAST(null);

				if(argsRest.produced())
					args.addAll(((AbsArgs)argsRest).args());

				return new AbsArgs(n, args); 
			}))
		);
	}

	// ArgsRest		--------------------------------------------------------- //
	//
	// ArgsRest --> , Expr ArgsRest | eps
	//
	private DerNode ArgsRest() {
		N n = new N(ArgsRest);

		nullable();
		return n.nret(
			n.ft(COMMA) || n.n(Expr()) || n.n(ArgsRest()) || n.ast
			(no_lsub -> {
				Vector<AbsExpr> args = new Vector<>();
				args.add((AbsExpr)n.sub(1).toAST(null));
				AbsTree argsRest = n.sub(2).toAST(null);

				if(argsRest.produced())
					args.addAll(((AbsArgs)argsRest).args());

				return new AbsArgs(n, args); 
			})
		);
	}

	// CastEps		--------------------------------------------------------- //
	//
	// CastEps --> : Type | eps
	//
	private DerNode CastEps() {
		N n = new N(CastEps);
		
		nullable();
		return n.nret(
			(n.ft(COLON) || n.n(Type()) || n.ast
			(lsub -> {
				AbsType type = (AbsType)n.sub(1).toAST(null);

				return new AbsCastExpr(
					new Location(lsub, n),
					(AbsExpr)lsub,
					type
				);
			}))
		);
	}

	// WhereEps		--------------------------------------------------------- //
	//
	// WhereEps --> WHERE Decl Decls | eps
	//
	private DerNode WhereEps() {
		N n = new N(WhereEps);

		nullable();
		return n.nret(
			(n.ft(WHERE) || n.n(Decl()) || n.n(Decls()) || n.ast
			(no_lsub -> {
				Vector<AbsDecl> decls = new Vector<>();
				decls.add((AbsDecl)n.sub(1).toAST(null));

				AbsTree declsRest = n.sub(2).toAST(null);
				if(declsRest.produced())
					decls.addAll(((AbsDecls)declsRest).decls());
				
				return new AbsDecls(
					new Location(decls.firstElement(), decls.lastElement()),
					decls
				);
			}))
		);
	}

	// Stmts		--------------------------------------------------------- //
	//
	// Stmts --> Stmt Stmts | eps
	//
	private DerNode Stmts() {
		N n = new N(Stmts);

		nullable();
		return n.nret(
			(n.n(Stmt()) || n.n(Stmts()) || n.ast
			(no_lsub -> {
				Vector<AbsStmt> stmts = new Vector<>();
				stmts.add((AbsStmt)n.sub(0).toAST(null));

				AbsTree stmtsRest = n.sub(1).toAST(null);
				if(stmtsRest.produced())
					stmts.addAll(((AbsStmts)stmtsRest).stmts());

				return new AbsStmts(
					new Location(stmts.firstElement(), stmts.lastElement()),
					stmts
				);
			}))
		);
	}

	// Stmt			--------------------------------------------------------- //
	//
	// Stmt --> Expr AssignEps
	// Stmt --> IF Expr THEN Stmt Stmts ElseEps END ; 
	// Stmt --> WHILE Expr DO Stmt Stmts END ;
	// 
	private DerNode Stmt() {
		N n = new N(Stmt);

		return n.ret(
			(n.ft(IF) || n.n(Expr()) || n.t(THEN) || n.n(Stmt()) || n.n(Stmts()) ||
			 n.n(ElseEps()) || n.t(END) || n.t(SEMIC) || n.ast
			(no_lsub -> {
				AbsExpr cond = (AbsExpr)n.sub(1).toAST(null);
				
				Vector<AbsStmt> thenStmtsVec = new Vector<>();
				thenStmtsVec.add((AbsStmt)n.sub(3).toAST(null));

				AbsTree thenStmtsRest = n.sub(4).toAST(null);
				if(thenStmtsRest.produced())
					thenStmtsVec.addAll(((AbsStmts)thenStmtsRest).stmts());
	
				AbsStmts thenStmts = new AbsStmts(
					new Location(thenStmtsVec.firstElement(), thenStmtsVec.lastElement()),
					thenStmtsVec
				);

				Vector<AbsStmt> elseStmtsVec = new Vector<>();

				AbsTree elseStmtsRest = n.sub(5).toAST(null);
				AbsStmts elseStmts;

				if(elseStmtsRest.produced()) {
					elseStmtsVec.addAll(((AbsStmts)elseStmtsRest).stmts());
					elseStmts = new AbsStmts(
						new Location(elseStmtsVec.firstElement(), elseStmtsVec.lastElement()),
						elseStmtsVec
					);
				}
				else {
					// No statements
					elseStmts = new AbsStmts(
						new Location(0,0, 0,0),
						elseStmtsVec
					);
				}

				return new AbsIfStmt(n, cond, thenStmts, elseStmts);
			})) &&

			(n.ft(WHILE) || n.n(Expr()) || n.t(DO) || n.n(Stmt()) || n.n(Stmts()) ||
			 n.t(END) || n.t(SEMIC) || n.ast
			(no_lsub -> {
				AbsExpr cond = (AbsExpr)n.sub(1).toAST(null);

				Vector<AbsStmt> stmts = new Vector<>();
				stmts.add((AbsStmt)n.sub(3).toAST(null));
	
				AbsTree stmtsRest = n.sub(4).toAST(null);
				if(stmtsRest.produced())
					stmts.addAll(((AbsStmts)stmtsRest).stmts());
				
				AbsStmts whileStmts = new AbsStmts(
					new Location(stmts.firstElement(), stmts.lastElement()),
					stmts
				);

				return new AbsWhileStmt(n, cond, whileStmts);
			})) &&

			// This is basically either an assignment or a singular expression
			(n.n(Expr()) || n.n(AssignEps()) || n.ast
			(no_lsub -> {
				AbsExpr expr = (AbsExpr)n.sub(0).toAST(null);
				AbsStmt stmt = (AbsStmt)n.sub(1).toAST(expr);

				return stmt;
			}))
		);
	}

	// AssignEps	--------------------------------------------------------- //
	//
	// AssignEps --> = Expr ; | ;
	//
	private DerNode AssignEps() {
		N n = new N(AssignEps);

		return n.ret(
			(n.ft(ASSIGN) || n.n(Expr()) || n.t(SEMIC) || n.ast
			(lsub -> {
				// Assignment
				AbsExpr src = (AbsExpr)n.sub(1).toAST(null);
				return new AbsAssignStmt(new Location(lsub, n), (AbsExpr)lsub, src);
			})) &&

			(n.ft(SEMIC) || n.ast
			(lsub -> {
				// Expression statement
				return new AbsExprStmt(n, (AbsExpr)lsub);
			}))
		
		);
	}

	// ElseEps		--------------------------------------------------------- //
	//
	// ElseEps --> ELSE Stmt Stmts | eps
	//
	private DerNode ElseEps() {
		N n = new N(ElseEps);

		nullable();
		return n.nret(
			(n.ft(ELSE) || n.n(Stmt()) || n.n(Stmts()) || n.ast
			(lsub -> {
				Vector<AbsStmt> stmts = new Vector<>();
				stmts.add((AbsStmt)n.sub(1).toAST(null));

				AbsTree stmtsRest = n.sub(2).toAST(null);
				if(stmtsRest.produced())
					stmts.addAll(((AbsStmts)stmtsRest).stmts());
				
				return new AbsStmts(
					new Location(stmts.firstElement(), stmts.lastElement()),
					stmts
				);
			}))
		);
	}

}
