# Self-help materials from the Grammar Anonymous Support Group

_There there, baby seal says that everything will be okay._

![happy place](./happy-place.jpg)

And now to the actual grammar.

# Terminals
Represented by the DerLeaf class (DerLeaf.java, provided in the template)

Defined by the Symbol.Term (Symbol.java)

    EOF             IDENTIFIER      IOR             XOR             AND             
    EQU             NEQ             LTH             LEQ             GTH             
    GEQ             ADD             SUB             MUL             DIV             
    MOD             NOT             ADDR            DATA            NEW             
    DEL             ASSIGN          COLON           COMMA           DOT             
    SEMIC           LBRACE          RBRACE          LBRACKET        RBRACKET        
    LPARENTHESIS    RPARENTHESIS    VOIDCONST       BOOLCONST       CHARCONST       
    INTCONST        STRCONST        PTRCONST        VOID            BOOL            
    CHAR            INT             PTR             ARR             REC             
    DO              ELSE            END             FUN             IF              
    THEN            TYP             VAR             WHERE           WHILE           

# Nonterminals
Represented by the DerNode class (DerNode.java, provided in the template)

Defined by DerNode.Nont

    Source          Decls           DeclsRest*      Decl            ParDeclsEps
    ParDecls        ParDeclsRest*   ParDecl*        BodyEps         Type
    CompDecls       CompDeclsRest*  CompDecl*       Expr            DisjExpr
    DisjExprRest    ConjExpr        ConjExprRest    RelExpr         RelExprRest
    AddExpr         AddExprRest     MulExpr         MulExprRest     PrefExpr
    PstfExpr        PstfExprRest*   AtomExpr        CallEps         ArgsEps*
    Args            ArgsRest        CastEps         WhereEps        Stmts
    StmtsRest*      Stmt            AssignEps       ElseEps

\* not used in our grammar due to additional simplifications.

# Original syntax structure

The \\\|, \\\(, \\\), \\\[, \\\], \\\{, \\\} represent literal characters, instead of regex constructs.

    (program)                   source --> decl {decl} 
    (type declaration)          decl --> typ identifier : type ; 
    (variable declaration)      decl --> var identifier : type ; 
    (function declaration)      decl --> fun identifier \([identifier : type {,identifier : type}]\) : type [=expr] ; 
    (atomic type)               type --> void | bool | char | int 
    (array type)                type --> arr \[expr\] type 
    (record type)               type --> rec \(identifier : type {,identifier : type}\) 
    (pointer type)              type --> ptr type 
    (named type)                type --> identifier 
    (enclosed type)             type --> \(type\) 
    (literal)                   expr --> literal:void | literal:bool | literal:char | literal:int | literal:pointer | literal:string 
    (unary expression)          expr --> unop expr 
    (binary expression)         expr --> expr binop expr 
    (variable access)           expr --> identifier 
    (function call )            expr --> identifier \([expr {,expr }]\) 
    (element access)            expr --> expr \[expr\] 
    (component access)          expr --> expr .identifier 
    (memory allocation)         expr --> new \(type\) 
    (memory deallocation)       expr --> del \(expr\) 
    (compound expression)       expr --> \{ stmt {stmt}:expr [where decl {decl }]\} 
    (typecast)                  expr --> \(expr :type\) 
    (enclosed expression)       expr --> \(expr\) 
    (expression)                stmt --> expr ; 
    (assignment)                stmt --> expr = expr ; 
    (conditional )              stmt --> if expr then stmt {stmt} [else stmt {stmt}] end ; 
    (loop)                      stmt --> while expr do stmt {stmt} end ;
    (unary operator)            unop --> ! | + | - | $ | @
    (binary operator)           binop --> \| | ^ | & | == | != | <= | >= | < | > | + | - | * | / | %

---

Representation of the above grammar with enums.
Some nonterminals have to be replaced with more specific nonterminals + unop & binop are not expanded yet.
Punctuation / delimiter terminals have been left as-is for better readability.

    (program)                   Source --> Decl {Decl} 
    (type declaration)          Decl --> TYP IDENTIFIER : Type ; 
    (variable declaration)      Decl --> VAR IDENTIFIER : Type ; 
    (function declaration)      Decl --> FUN IDENTIFIER \([IDENTIFIER : Type {,IDENTIFIER : Type}]\) : Type [=Expr] ; 
    (atomic type)               Type --> VOID | BOOL | CHAR | INT 
    (array type)                Type --> ARR \[Expr\] Type 
    (record type)               Type --> REC \(IDENTIFIER : Type {,IDENTIFIER : Type}\) 
    (pointer type)              Type --> PTR Type 
    (named type)                Type --> IDENTIFIER 
    (enclosed type)             Type --> \(Type\) 
    (literal)                   Expr --> VOIDCONST | BOOLCONST | CHARCONST | INTCONST | PTRCONST | STRCONST  
    (unary expression)          Expr --> unop Expr 
    (binary expression)         Expr --> Expr binop Expr 
    (variable access)           Expr --> IDENTIFIER 
    (function call )            Expr --> IDENTIFIER \([Expr {,Expr }]\) 
    (element access)            Expr --> Expr \[Expr\] 
    (component access)          Expr --> Expr . IDENTIFIER 
    (memory allocation)         Expr --> NEW \(Type\) 
    (memory deallocation)       Expr --> DEL \(Expr\) 
    (compound expression)       Expr --> \{ Stmt {Stmt}:Expr [WHERE Decl {Decl }]\} 
    (typecast)                  Expr --> \(Expr : Type\) 
    (enclosed expression)       Expr --> \(Expr\) 
    (expression)                Stmt --> Expr ; 
    (assignment)                Stmt --> Expr = Expr ; 
    (conditional )              Stmt --> IF Expr THEN Stmt {Stmt} [ELSE Stmt {Stmt}] END ; 
    (loop)                      Stmt --> WHILE Expr DO Stmt {Stmt} END ;
    (unary operator)            unop --> NOT | ADD | SUB | DATA | ADDR
    (binary operator)           binop --> IOR | XOR | AND | EQU | NEQ | LEQ | GEQ | LTH | GTH | ADD | SUB | MUL | DIV | MOD

---

As already determined on the lectures, one has to provide such grammar that every
nonterminal can be expanded into exactly one production based on the terminal on
the input. It is also valid if the production is epsilon, provided that this is
the only choice to expand the nonterminal.

That is, one does not actually have a choice to expand to either a non-epsilon
sequence or epsilon.

If one would choose an epsilon expansion where one could use a non-epsilon
expansion, no production will be applicable for the rest of the sequence.


Or, if we look from the perspective of the parsing table:

T[A,a] contains the rule A --> w **if and only if**

    a is in Fi(w) or
    ε is in Fi(w) and a is in Fo(A).

---

We also need to provide a proper precedence for unop & binop expressions:

    the lowest precedence

    | ^
    &
    == != <= >= < >
    + - (binary + and -)
    * / %
    ! + - $ @ new del type-cast (unary + and -)
    array-access component-access

    the highest precedence

---

Read the following:

https://en.wikipedia.org/wiki/LL_parser \
http://www.cs.unc.edu/~prins/Classes/520/Slides/07-Precedence.pdf \
https://courses.engr.illinois.edu/cs421/fa2016/lectures/19-parsing-amb-grm-2x3.pdf \
https://courses.engr.illinois.edu/cs421/sp2013/lectures/lecture9.pdf \
http://www.montefiore.ulg.ac.be/~geurts/Cours/compil/2012/03-syntaxanalysis-2-2012-2013.pdf

The main points:
- Bottom up evaluation of expressions in AST – therefore nodes lower in the tree are evaluated before their parents
- Operators of highest precedence evaluated first (bind more tightly)
- Higher precedence translates to longer derivation chain 
- When two or more recursive calls would be natural
  - keep right-most one for right assoicativity
  - keep left-most one for left assoiciativity

        <exp>      ::= 0 | 1  | <exp> + <exp> | <exp> * <exp>

        becomes
        
        <exp>      ::= <mult_exp> | <exp> + <mult_exp>
        <mult_exp> ::= <id> | <mult_exp> * <id>
        <id> ::= 0 | 1 

        (this is also called _stratified grammar_)
        (this particular grammar is suitable for the LL1 parser)

        An example that we also did at the lectures:

        E -> E + T // left recursion that needs to be resolved
        T -> T * F // left recursion
        F -> A
        F -> +A
        A -> id | ( E )

In this regard, all higher priorty expressions start as a low priority
nonterminals that eventually get decomposed into higher priority nonterminals
and those eventually into terminals.

Higher priority nonterminals will therefore be lower in the derivation tree and
as since we are doing LL1 parsing (always expanding the leftmost first), the
leftmost deepest nodes are going to be evaluated first.

The earlier a node is expanded, the higher it is in the tree, and therefore
the later it will get evaluated.

---

An additional note on associativity:

If we have binary operations of same priority, we assume left associativity in
our grammar - the expressions are to be evaluated left to right.

In the terms of derivations trees, the leftmost operand should be encountered
first. Since we are doing a left recursive descent, the only thing of importance
is that the derivation subtree containing the leftmost operand is left of the
subtree of the next operand.

This is described by both the grammar

I.

    E -> E '+' T
    E -> T

    (which is not suitable for LL1 due to left recursion)

and the grammar

II.

    E -> T Z
    Z -> '+' T Z
    Z -> ε

    (which has left recursion removed)

Both grammars follow left associativity, the difference is that the
- I. grammar derivation tree will be hanging left (the leftmost node deepest)
- II. grammar derivation tree will be hanging right (the rightmost node deepest)

See https://en.wikipedia.org/wiki/Left_recursion#Pitfalls for a graphical
representation.

---

A note the actual parser & what to expect from it:

The recursive descent parser, modeled after the LL1 stack based parser,
has two choices when expanding a nonterminal for the given input terminal:

- it can choose a production - in recusrive descent, a switch statement where a
  case chooses based on first terminal which following sequence of terminals
  to expect & nonterminal functions to call
- it can choose a nonterminal or epsilon, but _only_ after no other production
  matches - in recursive descent, the default statement in the switch, which is
  either empty (eps) or calls other nonterminal function

Since choosing eps / nonterminal is the last ditch try, we can be sure that
productions that start with terminals get matched before we get into expanding
prioritzed expressions.

So we can define non-operator language constructs at the same level as the lowest
priority operator. Because most of these start with a terminal, they will be
selected before the operator production. However, we need to be careful that
this doesn't break priorities - having `Expr --> Expr binop Expr` and `Expr --> IDENTIFIER `
at the same level will always choose the direct IDENTIFIER expansion over the binop expression.

---
---

We first develop the grammar for unop / binop expressions.

One thing to note here - if you look at the language as a whole, there are
constructs that need to be in place before an operand-composed expression can
be written - a function definiton at least.

However, for the sake of easier grammar development, we will ignore these for now
and concern ourselves only with operators.


For all non-operator expressions that can be part of an expression are assumed
to have a higher priority, since they need to "stick together" even if they are 
part of an expression.

    So 1 + test(b) is assumed to be decomposed as [[1] + [test([b])]]

We start with the lowest precedence, from the Expr nonterminal.

---
## | ^

    productions:
    (binary operator)           binop --> IOR | XOR
    (binary expression)         expr --> expr binop expr 

    starting nonterm:
    Expr

    higher priority nonterms:
    ConjExpr

In order to enforce left associativity, only the left operand can expand to
same level nonterminal.

    ----------------------------------------------------------------------------
    grammar:
    Expr --> DisjExpr
    DisjExpr --> DisjExpr IOR ConjExpr | DisjExpr XOR ConjExpr | ConjExpr

    ----------------------------------------------------------------------------
    LL1 grammar:
    Expr --> DisjExpr
    (
        left recursion removal
        E -> E '+' T
        E -> T

        this can also be represented as a regular expression T ('+' T)*

        this can be rewritten as
        E -> T Z
        Z -> '+' T Z
        Z -> ε

        in our case
        E => DisjExpr
        T => ConjExpr
        Z => DisjExprRest
    )
    DisjExpr --> ConjExpr DisjExprRest
    DisjExprRest --> IOR ConjExpr DisjExprRest | XOR ConjExpr DisjExprRest | eps


    Grammophone grammar:
    Expr -> DisjExpr .
    DisjExpr -> ConjExpr DisjExprRest .
    DisjExprRest -> IOR ConjExpr DisjExprRest .
    DisjExprRest -> XOR ConjExpr DisjExprRest .
    DisjExprRest -> .

---
## &

    productions:
    (binary operator)           binop --> AND
    (binary expression)         expr --> expr binop expr 

    starting nonterm:
    ConjExpr

    higher priority nonterms:
    RelExpr // as in "relational expression"

    ----------------------------------------------------------------------------
    grammar:
    ConjExpr --> ConjExpr AND RelExpr | RelExpr

    ----------------------------------------------------------------------------
    LL1 grammar:
    (left recursion removal)
    ConjExpr --> RelExpr ConjExprRest
    ConjExprRest --> AND RelExpr ConjExprRest | eps


    Grammophone grammar:
    ConjExpr -> RelExpr ConjExprRest .
    ConjExprRest -> AND RelExpr ConjExprRest .
    ConjExprRest -> .

---
## == != <= >= < >

Do note the following rule:
Relational operators are non-associative, all other binary operators are left associative.

So the relational expression shouldn't produce an additional relational expression nonterminal
(which would allow consecutive operands).

    productions:
    (binary operator)           binop --> EQU | NEQ | LEQ | GEQ | LTH | GTH
    (binary expression)         expr --> expr binop expr 

    starting nonterm:
    RelExpr

    higher priority nonterms:
    AddExpr

    ----------------------------------------------------------------------------
    grammar:
    RelExpr --> AddExpr EQU AddExpr | AddExpr NEQ AddExpr | AddExpr LEQ AddExpr |
                AddExpr GEQ AddExpr | AddExpr LTH AddExpr | AddExpr GTH AddExpr |
                AddExpr

    ----------------------------------------------------------------------------
    LL1 grammar:
    (left factoring)
    RelExpr --> AddExpr RelExprRest
    RelExprRest --> EQU AddExpr | NEQ AddExpr | LEQ AddExpr | GEQ AddExpr |
                    LTH AddExpr | GTH AddExpr | eps


    Grammophone grammar:
    RelExpr -> AddExpr RelExprRest .
    RelExprRest -> EQU AddExpr .
    RelExprRest -> NEQ AddExpr .
    RelExprRest -> LEQ AddExpr .
    RelExprRest -> GEQ AddExpr .
    RelExprRest -> LTH AddExpr .
    RelExprRest -> GTH AddExpr .
    RelExprRest -> .

---
## + - (binary + and -)

We again have left associativity rules, so the nonterm should produce itself.

    productions:
    (binary operator)           binop --> ADD | SUB
    (binary expression)         expr --> expr binop expr 

    starting nonterm:
    AddExpr

    higher priority nonterms:
    MulExpr

    ----------------------------------------------------------------------------
    grammar:
    AddExpr --> AddExpr ADD MulExpr | AddExpr SUB MulExpr | MulExpr

    ----------------------------------------------------------------------------
    LL1 grammar:
    (left recursion removal)
    AddExpr --> MulExpr AddExprRest
    AddExprRest --> ADD MulExpr AddExprRest | SUB MulExpr AddExprRest | eps


    Grammophone grammar:
    AddExpr -> MulExpr AddExprRest .
    AddExprRest -> ADD MulExpr AddExprRest .
    AddExprRest -> SUB MulExpr AddExprRest .
    AddExprRest -> .

---
## * / %

    productions:
    (binary operator)           binop --> MUL | DIV | MOD
    (binary expression)         expr --> expr binop expr 

    starting nonterm:
    MulExpr

    higher priority nonterms:
    PrefExpr

    ----------------------------------------------------------------------------
    grammar:
    MulExpr --> MulExpr MUL PrefExpr | MulExpr DIV PrefExpr |
                MulExpr MOD PrefExpr | MulExpr

    ----------------------------------------------------------------------------
    LL1 grammar:
    (left recursion removal)
    MulExpr --> PrefExpr MulExprRest
    MulExprRest --> MUL PrefExpr MulExprRest | DIV PrefExpr MulExprRest |
                    MOD PrefExpr MulExprRest | eps


    Grammophone grammar:
    MulExpr -> PrefExpr MulExprRest .
    MulExprRest -> MUL PrefExpr MulExprRest .
    MulExprRest -> DIV PrefExpr MulExprRest .
    MulExprRest -> MOD PrefExpr MulExprRest .
    MulExprRest -> .

---
## ! + - $ @ new del type-cast (unary + and -)
Do note that the unary +, - don't need any special treatment here since if the
context allowed for a binary +, - it would be expanded already.

We however need to be aware of the typecast & enclosed expression since they
start the same.

Other operators are directly determined by their terminal.

Operators that enclose their operands have same function as the enclosed
expression - the expression can again be of lowest priority ( Expr ).

Also, in order to simplify the grammar, we can slightly diverge from the
precedence in the specification. The specification declares the typecast to be
a level higher than access operators, however it would be more convenient to
handle it at the same level as the enclosed expression, which requires highest
priority (this way we can remove the first set clash by left factorization).

Since the typecast is not meant to be used with the access operators,
we will need to explicitly set up the grammar to not expand the possible 
access operators in that case.


    productions:
    (unary operator)            unop --> NOT | ADD | SUB | DATA | ADDR
    (unary expression)          Expr --> unop Expr 
    (memory allocation)         Expr --> NEW \(Type\) 
    (memory deallocation)       Expr --> DEL \(Expr\) 

    starting nonterm:
    PrefExpr

    higher priority nonterms:
    AtomExpr

    We have `PrefExpr --> unop PrefExpr` so that we can parse multiple
    consecutive operators in the likes of ( !!, ++, --, etc. )

    ----------------------------------------------------------------------------
    grammar:
    PrefExpr -->    NOT PrefExpr | ADD PrefExpr | SUB PrefExpr | DATA PrefExpr |
                    ADDR PrefExpr |
                    NEW \(Type\) | DEL \(Expr\) |
                    AtomExpr

    ----------------------------------------------------------------------------
    LL1 grammar:
    PrefExpr -->    NOT PrefExpr | ADD PrefExpr | SUB PrefExpr | DATA PrefExpr |
                    ADDR PrefExpr |
                    NEW \(Type\) | DEL \(Expr\) |
                    AtomExpr


    Grammophone grammar:
    PrefExpr -> NOT PrefExpr .
    PrefExpr -> ADD PrefExpr .
    PrefExpr -> SUB PrefExpr .
    PrefExpr -> DATA PrefExpr .
    PrefExpr -> ADDR PrefExpr .
    PrefExpr -> NEW ( Type ) .
    PrefExpr -> DEL ( Expr ) .
    PrefExpr -> AtomExpr .

---
## array-access component-access

We will assume that the unenclosed nonterminal signifies a **non-operator expression** -
this has higher priority than any **operator expression**
(as non-operator constructs should bind more thightly than the surrounding operators).

Enclosed operands can be of lowest priority ( Expr ) .

This is the highest priority / lowest level of the derivation tree.

    productions:
    (element access)            Expr --> Expr \[Expr\] 
    (component access)          Expr --> Expr . IDENTIFIER 

    // ## non-operator expressions ##
    (typecast)                  Expr --> \(Expr : Type\)
    (enclosed expression)       Expr --> \(Expr\) 
    (literal)                   Expr --> VOIDCONST | BOOLCONST | CHARCONST | INTCONST | PTRCONST | STRCONST  
    (variable access)           Expr --> IDENTIFIER 
    (function call )            Expr --> IDENTIFIER \([Expr {,Expr }]\) 
    (compound expression)       Expr --> \{ Stmt {Stmt}:Expr [WHERE Decl {Decl }]\} 

    starting nonterm:
    PstfExpr

    higher priority nonterms:
    -none-

We need to rework the original "PstfExpr --> Expr X | Expr" production here.

Think about it like this - it does not make sense if Expr can eventually reduce to Expr
(without additional nonterminals/terminals) - this provides two ways to get to a nonterminal
without consuming any input.

If the operand to operator wasn't handled by none of the preceding productions, than
we can assume that the operand has to be of at least the same or higher priority.

If we allow any following nonterminal produce an un-enclosed lower priority /
higher level nonterminal, we broke priorities (+ possibly introduced indirect left recursion).

    ----------------------------------------------------------------------------
    grammar:

    AtomExpr --> non-operator-expression PstfExpr
    PstfExpr --> \[ Expr \] PstfExpr | . IDENTIFIER PstfExpr | eps

    non-operator-expression -->
        \(Expr : Type\) |
        \(Expr\)

    non-operator-expression -->
        IDENTIFIER |
        IDENTIFIER \([Expr {,Expr }]\)
    
    non-operator-expression -->
        VOIDCONST | BOOLCONST | CHARCONST | INTCONST | PTRCONST | STRCONST |
        \{ Stmt {Stmt}:Expr [WHERE Decl {Decl }]\} 

    ----------------------------------------------------------------------------
    grammar (1. stage)
    
    AtomExpr --> non-operator-expression PstfExpr
    PstfExpr --> \[ Expr \] PstfExpr | . IDENTIFIER PstfExpr | eps

    (
        left factorization

        we've left factored the "\( Expr" together as this is probably how
        the original grammar got constructed
    )
    non-operator-expression --> \( Expr CastEps \)
    CastEps --> : Type | eps

    (left factorization)
    non-operator-expression --> IDENTIFIER CallEps
    CallEps --> \([Expr {,Expr }]\) | eps

    non-operator-expression -->
        VOIDCONST | BOOLCONST | CHARCONST | INTCONST | PTRCONST | STRCONST |
        \{ Stmt {Stmt}:Expr [WHERE Decl {Decl }]\} 

While the typecast was moved a level lower (thus declared as higher priority),
it does not cause issues with our language definition.

That's because the typecast actually encloses the expression it casts, and
therefore plays nicely with access operators `\(Expr : Type\) \[ Expr \]`.
Compared to C, where casts prefix expressions, something like `(int*)[5]` is
not syntactically correct and would therefore have to be handled by the grammar.

Some productions like VOIDCONST[ 5 ] don't exactly make sense
from type point of view, but that's up to the type checker to figure out.

    ----------------------------------------------------------------------------
    grammar (2. stage)

    AtomExpr --> non-operator-expression PstfExpr
    PstfExpr --> \[ Expr \] PstfExpr | . IDENTIFIER PstfExpr | eps

    non-operator-expression --> \( Expr CastEps \)
    CastEps --> : Type  | eps

    non-operator-expression --> IDENTIFIER CallEps
    CallEps --> \([Expr {,Expr }]\) | eps

    non-operator-expression -->
        VOIDCONST | BOOLCONST | CHARCONST |
        INTCONST | PTRCONST | STRCONST |
        \{ Stmt {Stmt}:Expr [WHERE Decl {Decl }]\}

Expand repetitions

    ----------------------------------------------------------------------------
    grammar (3. stage)

    AtomExpr --> non-operator-expression PstfExpr
    PstfExpr --> \[ Expr \] PstfExpr | . IDENTIFIER PstfExpr | eps

    non-operator-expression --> \( Expr CastEps \)
    CastEps --> : Type | eps

    (expand repetition)
    non-operator-expression --> IDENTIFIER CallEps
    CallEps --> \( Args \) | eps
    Args --> Expr ArgsRest | eps
    ArgsRest --> , Expr ArgsRest | eps

    non-operator-expression -->
        VOIDCONST | BOOLCONST | CHARCONST |
        INTCONST | PTRCONST | STRCONST

    (expand repetition)
    non-operator-expression --> \{ Stmt Stmts : Expr WhereEps \}
    Stmts --> Stmt Stmts | eps                          // ## We reuse this for (conditional) (loop) ##
    WhereEps --> WHERE Decl Decls | eps                 // ## We reuse this for (program) ##
    Decls --> Decl Decls | eps

Replace `non-operator-expression` with `AtomExpr` & reorder:

    ----------------------------------------------------------------------------
    grammar (4. stage)

    AtomExpr --> \( Expr CastEps \) PstfExpr
    CastEps --> : Type | eps

    AtomExpr --> IDENTIFIER CallEps PstfExpr
    CallEps --> \( Args \) | eps
    Args --> Expr ArgsRest | eps
    ArgsRest --> , Expr ArgsRest | eps

    AtomExpr -->
        VOIDCONST PstfExpr | BOOLCONST PstfExpr | CHARCONST PstfExpr |
        INTCONST PstfExpr  | PTRCONST PstfExpr  | STRCONST PstfExpr

    AtomExpr --> \{ Stmt Stmts : Expr WhereEps \} PstfExpr
    Stmts --> Stmt Stmts | eps                          // ## We reuse this for (conditional) (loop) ##
    WhereEps --> WHERE Decl Decls | eps                 // ## We reuse this for (program) ##
    Decls --> Decl Decls | eps

    PstfExpr --> \[ Expr \] PstfExpr | . IDENTIFIER PstfExpr | eps


    Grammophone grammar:
    AtomExpr -> \( Expr CastEps \) PstfExpr .
    CastEps -> : Type  .
    CastEps -> .

    AtomExpr -> IDENTIFIER CallEps PstfExpr .
    CallEps -> \( Args \) .
    CallEps -> .
    Args -> Expr ArgsRest .
    Args -> .
    ArgsRest -> , Expr ArgsRest .
    ArgsRest -> .

    AtomExpr -> VOIDCONST PstfExpr .
    AtomExpr -> BOOLCONST PstfExpr .
    AtomExpr -> CHARCONST PstfExpr .
    AtomExpr -> INTCONST PstfExpr .
    AtomExpr -> PTRCONST PstfExpr .
    AtomExpr -> STRCONST PstfExpr .

    AtomExpr -> \{ Stmt Stmts : Expr WhereEps \} PstfExpr .
    Stmts -> Stmt Stmts .
    Stmts -> .
    WhereEps -> WHERE Decl Decls .
    WhereEps -> .
    Decls -> Decl Decls .
    Decls -> .

    PstfExpr -> \[ Expr \] PstfExpr .
    PstfExpr -> DOT IDENTIFIER PstfExpr .
    PstfExpr -> .

---
## Non-operator constructs

These non-operator constructs are can't be part of expressions, therefore we
don't need to concern overselves over the priority or with how thightly they
should hold togeter.

Since most non-operator constructs start with a keyword (which is a terminal),
the productions are trivial.

    productions:
    (program)                   Source --> Decl {Decl} 
    (type declaration)          Decl --> TYP IDENTIFIER : Type ; 
    (variable declaration)      Decl --> VAR IDENTIFIER : Type ; 
    (function declaration)      Decl --> FUN IDENTIFIER \([IDENTIFIER :Type {,IDENTIFIER :Type}]\):Type [=Expr] ; 
    (atomic type)               Type --> VOID | BOOL | CHAR | INT 
    (array type)                Type --> ARR \[Expr\] Type 
    (record type)               Type --> REC \(IDENTIFIER : Type {,IDENTIFIER : Type}\) 
    (pointer type)              Type --> PTR Type 
    (named type)                Type --> IDENTIFIER 
    (enclosed type)             Type --> \(Type\) 
    (expression)                Stmt --> Expr ; 
    (assignment)                Stmt --> Expr = Expr ; 
    (conditional)               Stmt --> IF Expr THEN Stmt {Stmt} [ELSE Stmt {Stmt}] END ; 
    (loop)                      Stmt --> WHILE Expr DO Stmt {Stmt} END ;

    ----------------------------------------------------------------------------
    grammar:
    Source --> Decl {Decl} 
    Decl --> TYP IDENTIFIER : Type ; 
    Decl --> VAR IDENTIFIER : Type ; 
    Decl --> FUN IDENTIFIER \([IDENTIFIER :Type {,IDENTIFIER :Type}]\):Type [=Expr] ; 
    Type --> VOID | BOOL | CHAR | INT 
    Type --> ARR \[Expr\] Type 
    Type --> REC \(IDENTIFIER : Type {,IDENTIFIER : Type}\) 
    Type --> PTR Type 
    Type --> IDENTIFIER 
    Type --> \(Type\) 
    Stmt --> Expr ; 
    Stmt --> Expr = Expr ; 
    Stmt --> IF Expr THEN Stmt {Stmt} [ELSE Stmt {Stmt}] END ; 
    Stmt --> WHILE Expr DO Stmt {Stmt} END ;

    ----------------------------------------------------------------------------
    
    Source --> Decl Decls
    Decl --> TYP IDENTIFIER : Type ; 
    Decl --> VAR IDENTIFIER : Type ; 

    (expand repetition)
    Decl --> FUN IDENTIFIER \( ParDeclsEps \) : Type BodyEps ; 
    ParDeclsEps --> IDENTIFIER : Type ParDecls | eps
    ParDecls --> , IDENTIFIER : Type ParDecls | eps     // ## We reuse this for (record type) ##
    BodyEps --> = Expr | eps
    
    Type --> VOID | BOOL | CHAR | INT 
    Type --> ARR \[Expr\] Type 

    (expand repetition)     // ## Even though we could use ParDecls,
                            // ## using separate nonterminal will
                            // ## simplify AST generation.
    Type --> REC \(IDENTIFIER : Type CompDecls\)
    CompDecls --> , IDENTIFIER : Type | eps

    Type --> PTR Type 
    Type --> IDENTIFIER 
    Type --> \( Type \) 

    (left factorization)
    Stmt --> Expr AssignEps ;
    AssignEps --> = Expr | eps

    (expand repetition)
    Stmt --> IF Expr THEN Stmt Stmts ElseEps END ; 
    ElseEps --> ELSE Stmt Stmts | eps

    (expand repetition)
    Stmt --> WHILE Expr DO Stmt Stmts END ;

    ----------------------------------------------------------------------------
    LL1 grammar:

    Source --> Decl Decls
    Decl --> TYP IDENTIFIER : Type ; 
    Decl --> VAR IDENTIFIER : Type ; 

    Decl --> FUN IDENTIFIER \( ParDeclsEps \) : Type BodyEps ; 
    ParDeclsEps --> IDENTIFIER : Type ParDecls | eps
    ParDecls --> , IDENTIFIER : Type ParDecls | eps     // ## We reuse this for (record type) ##
    BodyEps --> = Expr | eps

    Type --> VOID | BOOL | CHAR | INT 
    Type --> ARR \[Expr\] Type 

    Type --> REC \(IDENTIFIER : Type CompDecls\)
    CompDecls --> , IDENTIFIER : Type | eps

    Type --> PTR Type 
    Type --> IDENTIFIER 
    Type --> \( Type \) 

    (final nullable nonterminal expansion)
    ## In the cases where the last nonterminal in a derivation is nullable, it
    ## might be beneficial to move the remaining terminal symbols into the
    ## nonterminal.
    ##
    ## In some cases one can simplify AST construction, since the final object
    ## can be produced in the last nonterminal - if we parse nonterminals at
    ## this level, we can account their location in the object produced
    ## (Which is what we usually want)
    ##
    ## Do note that this is highly dependent on how a specific AST object
    ## gets constructed (which keywords / literals / constants it comprises of,
    ## and the order of them).
    ##
    Stmt --> Expr AssignEps
    AssignEps --> = Expr ; | ;

    Stmt --> IF Expr THEN Stmt Stmts ElseEps END ; 
    ElseEps --> ELSE Stmt Stmts | eps

    Stmt --> WHILE Expr DO Stmt Stmts END ;


    Grammophone grammar:
    Source -> Decl Decls .
    Decl -> TYP IDENTIFIER : Type ; .
    Decl -> VAR IDENTIFIER : Type ; .

    Decl -> FUN IDENTIFIER ( ParDeclsEps ) : Type BodyEps ; .
    ParDeclsEps -> IDENTIFIER : Type ParDecls .
    ParDeclsEps -> .
    ParDecls -> , IDENTIFIER : Type ParDecls .
    ParDecls -> .
    BodyEps -> = Expr .
    BodyEps -> .
    
    Type -> VOID .
    Type -> BOOL .
    Type -> CHAR .
    Type -> INT .
    Type -> ARR [ Expr ] Type .

    Type -> REC ( IDENTIFIER : Type ParDecls ) .

    Type -> PTR Type .
    Type -> IDENTIFIER .
    Type -> ( Type ) .

    Stmt -> Expr AssignEps .
    AssignEps -> = Expr ; .
    AssignEps -> ; .

    Stmt -> IF Expr THEN Stmt Stmts ElseEps END ; .
    ElseEps -> ELSE Stmt Stmts .
    ElseEps -> .

    Stmt -> WHILE Expr DO Stmt Stmts END ; .

--------------------------------------------------------------------------------

# TL;DR: just gimmie the grammar please

Ok, ok, here you go:

--------------------------------------------------------------------------------
### Standard LL1 grammar:

    Expr --> DisjExpr
    DisjExpr --> ConjExpr DisjExprRest
    DisjExprRest --> IOR ConjExpr DisjExprRest | XOR ConjExpr DisjExprRest | eps

    ConjExpr --> RelExpr ConjExprRest
    ConjExprRest --> AND RelExpr ConjExprRest | eps

    RelExpr --> AddExpr RelExprRest
    RelExprRest --> EQU AddExpr | NEQ AddExpr | LEQ AddExpr | GEQ AddExpr |
                    LTH AddExpr | GTH AddExpr | eps

    AddExpr --> MulExpr AddExprRest
    AddExprRest --> ADD MulExpr AddExprRest | SUB MulExpr AddExprRest | eps

    MulExpr --> PrefExpr MulExprRest
    MulExprRest --> MUL PrefExpr MulExprRest | DIV PrefExpr MulExprRest |
                    MOD PrefExpr MulExprRest | eps

    PrefExpr -->    NOT AtomExpr | ADD AtomExpr | SUB AtomExpr | DATA AtomExpr |
                    ADDR AtomExpr |
                    NEW \(Type\) | DEL \(Expr\) |
                    AtomExpr

    AtomExpr --> \( Expr CastEps PstfExpr
    CastEps --> : Type \) | \)
    AtomExpr --> IDENTIFIER CallEps PstfExpr
    CallEps --> \(Args\) | eps
    Args --> Expr ArgsRest | eps                 // ## BEWARE ##
    ArgsRest --> , Expr ArgsRest | eps
    AtomExpr -->
        VOIDCONST PstfExpr | BOOLCONST PstfExpr | CHARCONST PstfExpr |
        INTCONST PstfExpr  | PTRCONST PstfExpr  | STRCONST PstfExpr
    AtomExpr --> \{ Stmt Stmts : Expr WhereEps \} PstfExpr
    Stmts --> Stmt Stmts | eps                   // ## BEWARE ##
    WhereEps --> WHERE Decl Decls | eps
    Decls --> Decl Decls | eps                   // ## BEWARE ##
    PstfExpr --> \[ Expr \] PstfExpr | . IDENTIFIER PstfExpr | eps

    Source --> Decl Decls                        // ## BEWARE ##
    Decl --> TYP IDENTIFIER : Type ; 
    Decl --> VAR IDENTIFIER : Type ; 
    Decl --> FUN IDENTIFIER \( ParDeclsEps \) : Type BodyEps; 
    ParDeclsEps --> IDENTIFIER : Type ParDecls | eps
    ParDecls --> , IDENTIFIER : Type ParDecls | eps
    BodyEps --> = Expr | eps
    Type --> VOID | BOOL | CHAR | INT 
    Type --> ARR \[Expr\] Type 
    Type --> REC \(IDENTIFIER : Type ParDecls\) 
    Type --> PTR Type 
    Type --> IDENTIFIER 
    Type --> \( Type \) 
    Stmt --> Expr AssignEps ;
    AssignEps --> = Expr | eps
    Stmt --> IF Expr THEN Stmt Stmts ElseEps END ; 
    ElseEps --> ELSE Stmt Stmts | eps
    Stmt --> WHILE Expr DO Stmt Stmts END ;

The lines marked with the scary `## BEWARE ##` are actually not that bad,
but put the programmer in a slight pickle when it comes to implementing the
grammar with the recursive descent algorithm.

Although the grammar itself is LL1, you will find that when you are trying to
expand one of the marked productions, you don't know at that point if you need
to choose the nonterminal expansion or epsilon.

![everything is fine](./all-is-fine.jpg)

However, we did not break any of the godly principles for LL1 parsing
(Grammophone wan't playing trick on you) - we do, however, require some tricks
to do this via recursive descent.

Point (and the main reason why Grammophone this still considers LL1) is, that
all marked nullable nonterminals, when choosing non-epsion production,
eventually produce a terminal (so we don't fall into infinite recursion).

The problem is that in the normal case, when you come to a nonterminal, where you
can't choose any of the productions, you consider this an error since none matched.
But in the case of a nullable nonterminal, you don't want to consider this
an error - it just means that we should have chosen epsilon instead of expanding
a non-nullable production.

We can make this work by having a flag that marks that we've encountered
a nullable nonterminal. This way, when we can't expand any of the productions
further down, we return instead of reporting an error.

However, we make use of this flag only for the first terminal in the production -
when we actually manage to match the first terminal, we've taken the non-nullable
production of the nullable nonterminal, and all further input should therefore
match, or otherwise be considered an error. So we should unset the flag after
matching the first terminal.

We should also be aware that when we're returning back to the nullable
nonterminal, we should skip expansion of all the intermediary productions -
we should return immediately from the method call. You can know that when a
method returns with the flag for encountered nullable terminal still being
asserted.

When you get back to the nullable nonterminal, you deassert the flag and return
(which is essentially choosing an epsilon production).

--------------------------------------------------------------------------------
### Grammophone variant:

    Expr -> DisjExpr .
    DisjExpr -> ConjExpr DisjExprRest .
    DisjExprRest -> IOR ConjExpr DisjExprRest .
    DisjExprRest -> XOR ConjExpr DisjExprRest .
    DisjExprRest -> .

    ConjExpr -> RelExpr ConjExprRest .
    ConjExprRest -> AND RelExpr ConjExprRest .
    ConjExprRest -> .

    PrefExpr -> NOT PrefExpr .
    PrefExpr -> ADD PrefExpr .
    PrefExpr -> SUB PrefExpr .
    PrefExpr -> DATA PrefExpr .
    PrefExpr -> ADDR PrefExpr .
    PrefExpr -> NEW ( Type ) .
    PrefExpr -> DEL ( Expr ) .
    PrefExpr -> AtomExpr .

    AddExpr -> MulExpr AddExprRest .
    AddExprRest -> ADD MulExpr AddExprRest .
    AddExprRest -> SUB MulExpr AddExprRest .
    AddExprRest -> .

    MulExpr -> PrefExpr MulExprRest .
    MulExprRest -> MUL PrefExpr MulExprRest .
    MulExprRest -> DIV PrefExpr MulExprRest .
    MulExprRest -> MOD PrefExpr MulExprRest .
    MulExprRest -> .

    AtomExpr -> \( Expr CastEps \) PstfExpr .
    CastEps -> : Type  .
    CastEps -> .
    AtomExpr -> IDENTIFIER CallEps PstfExpr .
    CallEps -> \( Args \) .
    CallEps -> .
    Args -> Expr ArgsRest .
    Args -> .
    ArgsRest -> , Expr ArgsRest .
    ArgsRest -> .
    AtomExpr -> VOIDCONST PstfExpr .
    AtomExpr -> BOOLCONST PstfExpr .
    AtomExpr -> CHARCONST PstfExpr .
    AtomExpr -> INTCONST PstfExpr .
    AtomExpr -> PTRCONST PstfExpr .
    AtomExpr -> STRCONST PstfExpr .
    AtomExpr -> \{ Stmt Stmts : Expr WhereEps \} PstfExpr .
    Stmts -> Stmt Stmts .
    Stmts -> .
    WhereEps -> WHERE Decl Decls .
    WhereEps -> .
    Decls -> Decl Decls .
    Decls -> .
    PstfExpr -> \[ Expr \] PstfExpr .
    PstfExpr -> DOT IDENTIFIER PstfExpr .
    PstfExpr -> .

    Source -> Decl Decls .
    Decl -> TYP IDENTIFIER : Type ; .
    Decl -> VAR IDENTIFIER : Type ; .
    Decl -> FUN IDENTIFIER ( ParDeclsEps ) : Type BodyEps ; .
    ParDeclsEps -> IDENTIFIER : Type ParDecls .
    ParDeclsEps -> .
    ParDecls -> , IDENTIFIER : Type ParDecls .
    ParDecls -> .
    BodyEps -> = Expr .
    BodyEps -> .
    Type -> VOID .
    Type -> BOOL .
    Type -> CHAR .
    Type -> INT .
    Type -> ARR [ Expr ] Type .
    Type -> REC ( IDENTIFIER : Type ParDecls ) .
    Type -> PTR Type .
    Type -> IDENTIFIER .
    Type -> ( Type ) .
    Stmt -> Expr AssignEps .
    AssignEps -> = Expr ; .
    AssignEps -> ; .
    Stmt -> IF Expr THEN Stmt Stmts ElseEps END ; .
    ElseEps -> ELSE Stmt Stmts .
    ElseEps -> .
    Stmt -> WHILE Expr DO Stmt Stmts END ; .

--------------------------------------------------------------------------------
# Notes regarding JSMachines

Originally I've also supported this tool, but it turned out that its
LL1 parser generator has issues with complex grammars.

The following grammar subset in JSMachines has issues

    Expr -> MulExpr

    MulExpr -> PstfExpr MulExprRest
    MulExprRest -> MUL PstfExpr MulExprRest
    MulExprRest -> ''

    PstfExpr -> VOIDCONST
    PstfExpr -> { Stmt Stmts : Expr WhereEps }

    WhereEps -> WHERE Decl CompDecls
    WhereEps -> ''

but Grammophone variant

    Expr -> MulExpr .

    MulExpr -> PstfExpr MulExprRest .
    MulExprRest -> MUL PstfExpr MulExprRest .
    MulExprRest -> .

    PstfExpr -> VOIDCONST .
    PstfExpr -> { Stmt Stmts : Expr WhereEps } .

    WhereEps -> WHERE Decl CompDecls .
    WhereEps -> .

seems to resolve without issue.
Also, some other LL1 parser generator at http://www.fit.vutbr.cz/~ikocman/llkptg/
again with its own syntax

    %token MUL VOIDCONST Stmt Stmts COLON LBRACE RBRACE WHERE Decl CompDecls
    %%
    Expr : MulExpr ;
    MulExpr : PstfExpr MulExprRest ;
    MulExprRest : MUL PstfExpr MulExprRest ;
    MulExprRest : ;
    PstfExpr : VOIDCONST ;
    PstfExpr : LBRACE Stmt Stmts COLON Expr WhereEps RBRACE ;
    WhereEps : WHERE Decl CompDecls ;
    WhereEps : ;

seems to parse the above example correctly.

So, due to lack of additional time for a deeper research, I would assume that
JSMachines has some issues. It's still useful for checking priorities,
but there be dragons and stuff.

--------------------------------------------------------------------------------

That's all folks!
