/**
 * @author sliva
 */
package compiler.phases.lexan;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import compiler.common.report.Location;
import compiler.common.report.Report;
import compiler.data.symbol.Symbol;
import compiler.phases.Phase;

/**
 * Lexical analysis.
 * 
 * @author sliva
 */
public class LexAn extends Phase {

	/** The name of the source file. */
	private final String
	srcFileName;

	/** Source file string */
	private String
	srcFile;

	/** Source file lines, starting index */
	private int
	srcFile_line_startOffset[];

	/**
	 * Cross-invocation preserved offset.
	 */
	int curOffset = 0;

	/**
	 * Cross-invocation Matcher.
	 */
	Matcher matcher;

	/**
	 * Constructs a new phase of lexical analysis.
	 */
	public LexAn() {
		super("lexan");

		srcFile = null; srcFile_line_startOffset = null;


		srcFileName = compiler.Main.cmdLineArgValue("--src-file-name");

		BufferedReader 
		srcFile_reader;
		
		try {srcFile_reader = new BufferedReader(new FileReader(srcFileName));}
		catch (IOException ___) {
			throw new Report.Error("Cannot open source file '" + srcFileName + "'.");
		}

		// read the source file + get line numbers
		{
			StringBuffer
			srcFile_buf = new StringBuffer();

			LinkedList<Integer>
			lineStartIdx_buf = new LinkedList<>();

			try {
				String currentLine;
				int currentLine_startOffset = 0;

				while
				(
					// readLine strips out OS-specific newline characters
					(currentLine = srcFile_reader.readLine()) != null
				) {
					lineStartIdx_buf.add(currentLine_startOffset);
					srcFile_buf.append(currentLine + "\n");

					currentLine_startOffset += currentLine.length() + 1;
				}
			}
			catch(IOException ___) {
				throw new Report.Error("Cannot read file '" + this.srcFileName + "'.");
			}

			srcFile = srcFile_buf.toString();

			srcFile_line_startOffset =
				lineStartIdx_buf.stream().mapToInt(i->i).toArray();
		}

		try {srcFile_reader.close();}
		catch (IOException ___)
			{Report.warning("Cannot close source file '" + this.srcFileName + "'.");}
		
		// We instantiate a matcher with a dummy pattern
		//
		// This is because the Matcher class has no public constructors,
		// yet we don't want to create a matcher for every pattern, as that
		// would require updating the start/end positions for all of them
		//
		matcher = Pattern.compile("").matcher(srcFile);
	}

	@Override
	public void close() {
		super.close();
	}

	/**
	 * Class which provides offset to line/column computation.
	 */
	class LocationGen {
		// all lines / columns are zero indexed here
		int
		startLine,
		endLine;
		
		int
		startColumn,
		endColumn;

		/**
		 * Cross-invocation preserved current line number.
		 * 
		 * Since we are parsing the file continuously, we can assume that the method
		 * call will provide us with an offset that is either equal or greater than
		 * the previous one.
		 */
		int curLineNo = 0;

		/**
		 * Get line number based on file offset.
		 * 
		 * Since we are parsing the file continuously, we can assume that the method
		 * call will provide us with an offset that is either equal or greater than
		 * the previous one.
		 */
		private int lineNumber(int offset){
			while(
				curLineNo+1 < srcFile_line_startOffset.length &&
				srcFile_line_startOffset[curLineNo+1] <= offset
			) curLineNo++;

			return curLineNo;
		}

		/** Offset range constructor */
		Location getLocation(int startOffset, int endOffset) {

			startLine 	= lineNumber(startOffset);
			// -1 to exclude first nonmatching char
			endLine 	= lineNumber(endOffset - 1);

			startColumn	= startOffset - srcFile_line_startOffset[startLine];
			endColumn	= endOffset - srcFile_line_startOffset[startLine] - 1;

			return new Location(startLine+1, startColumn+1, endLine+1, endColumn+1);
		}

		/** Single offset constructor */
		Location getLocation(int offset) {
			startLine 	= endLine 	= lineNumber(offset);
			startColumn = endColumn = offset - srcFile_line_startOffset[startLine];

			return new Location(startLine+1, startColumn+1, endLine+1, endColumn+1);
		}
	}
	LocationGen location = new LocationGen();

	/**
	 * The lexer.
	 * 
	 * This method returns the next symbol from the source file. To perform the
	 * lexical analysis of the entire source file, this method must be called until
	 * it returns EOF. This method calls {@link #lexify()}, logs its result if
	 * requested, and returns it.
	 * 
	 * @return The next symbol from the source file or EOF if no symbol is available
	 *         any more.
	 */
	public Symbol lexer() {
		Symbol symb = lexify();
		if (symb.token != Symbol.Term.EOF)
			symb.log(logger);
		return symb;
	}

	/**
	 * Performs the lexical analysis of the source file.
	 * 
	 * This method returns the next symbol from the source file. To perform the
	 * lexical analysis of the entire source file, this method must be called until
	 * it returns EOF.
	 * 
	 * @return The next symbol from the source file or EOF if no symbol is available
	 *         any more.
	 */
	private Symbol lexify() {
		
		// If the current offset starts with a comment char or whitespace,
		// skip until end of comment / non-whitespace
		//
		// The language has only single-line comments (#).
		//
		for(
			boolean inComment = false;

			curOffset < srcFile.length() &&
			(
				// Start of comment
				!inComment &&
				(inComment = srcFile.charAt(curOffset) == '#')
				||

				// Stay in single line comment until newline
				inComment &&
				(inComment = !(srcFile.charAt(curOffset) == '\n'))
				||

				// Non-comment whitespace
				!inComment &&
				(
					srcFile.charAt(curOffset) == ' ' ||
					srcFile.charAt(curOffset) == '\t' ||
					srcFile.charAt(curOffset) == '\n'
				)
			)
			;

			// Advance current offset until we pass the comments / whitespace
			curOffset++
		);

		// If we got to the end of the file, return EOF
		if(curOffset == srcFile.length()) {

			return new Symbol(
				Symbol.Term.EOF,
				"",
				location.getLocation(curOffset)
			);
		}

		Symbol matched = null;

		// Set up the regex matcher start offset to current offset
		matcher.region(curOffset, srcFile.length());

		// Since we might match either an identifier or some other symbol, we
		// can't move the offset at the first match
		int curEndOffset = curOffset;

		// We go through all possible terms and try to match their regexes
		//
		// The regexes should be set up so that there are no ambiguous matches
		// - that is, first positive match (with the exception of Identifiers)
		// represents the longest possible valid symbol
		// (so, input ">=" shoulf match GEQ, and not GTH, ASSIGN)
		//
		for(Symbol.Term term : Symbol.Term.values()) {
			// Set pattern & check if it matches from current offset
			matcher.usePattern(term.pattern);

			if(matcher.lookingAt()) {
				curEndOffset = matcher.end();

				matched = new Symbol(
					term,
					srcFile.substring(curOffset, curEndOffset),
					location.getLocation(curOffset, curEndOffset)
				);

				// Finish unless we matched an identifier, since in that
				// case we might still match a keyword / literal
				if(term != Symbol.Term.IDENTIFIER)
					break;
			}
		}
		if(matched == null)
		// If we didn't match any pattern, then we have a lexer error
		{
			// Since the Matcher class does not expose the last field in the
			// case of matcher failed, we can't get to know how far in the
			// pattern we've got.
			//
			// So let's at least provide some context.
			//
			int contextOffset =
				curOffset + 20 < srcFile.length() ?
				curOffset + 20 : srcFile.length();

			throw new Report.Error(
				location.getLocation(curOffset),
				String.format(
					"[file offset %d] sequence \"%s ...\" does not match any lexical element",
					curOffset,
					srcFile.substring(curOffset, contextOffset)
				)
			);
		}
		// Update offset to get to first nonmatched char
		curOffset = curEndOffset;

		return matched;
	}

}