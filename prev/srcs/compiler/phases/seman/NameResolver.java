/**
 * @author sliva
 */
package compiler.phases.seman;

import compiler.common.report.Locatable;
import compiler.common.report.Report;
import compiler.data.abstree.AbsBlockExpr;
import compiler.data.abstree.AbsCompDecl;
import compiler.data.abstree.AbsCompDecls;
import compiler.data.abstree.AbsDecl;
import compiler.data.abstree.AbsFunDecl;
import compiler.data.abstree.AbsFunDef;
import compiler.data.abstree.AbsFunName;
import compiler.data.abstree.AbsName;
import compiler.data.abstree.AbsParDecl;
import compiler.data.abstree.AbsRecExpr;
import compiler.data.abstree.AbsRecType;
import compiler.data.abstree.AbsSource;
import compiler.data.abstree.AbsTypDecl;
import compiler.data.abstree.AbsTypName;
import compiler.data.abstree.AbsVarDecl;
import compiler.data.abstree.AbsVarName;
import compiler.data.abstree.visitor.AbsFullVisitor;
import compiler.data.abstree.visitor.AbsNullVisitor;
import compiler.phases.seman.SymbTable.CannotFndNameException;
import compiler.phases.seman.SymbTable.CannotInsNameException;

/**
 * Name resolving: the result is stored in {@link SemAn#declaredAt}.
 * 
 * @author sliva
 * 
 * NameResolver implements AbsNullVisitor here, since, besides Source node
 * other AST nodes are traversed through other visitors.
 */
public class NameResolver extends AbsNullVisitor<Void, Void> {

	/** Symbol table. */
	private final SymbTable symbTable = new SymbTable();


	// Debug
	//
	// To enable debugging output
	private final boolean debug = false;

	int debug_depth = 0;
	final int debug_spaces = 4;

	String debug_prefix() {
		StringBuilder s = new StringBuilder();
		s.append(String.format("[%3d] ", debug_depth));
		for(int i = 0; i < debug_depth; i++)
			s.append(String.format(".%"+(debug_spaces-1)+"s", ""));
		return s.toString();
	}

	private void dbgDecl(String kind, AbsDecl decl) {
		System.out.format(
			"%-45s @ %s%n",

			String.format(
				"%s+ %-7s %-10s",
				debug_prefix(),
				kind, decl.name
			),

			decl.type.location()
		);
	}

	private void dbgUsage(String kind, AbsName name) {
		String hrname = "?";
		if (name instanceof AbsTypName)
			hrname = ((AbsTypName)name).name;
		
		if (name instanceof AbsVarName)
			hrname = ((AbsVarName)name).name;

		if (name instanceof AbsFunName)
			hrname = ((AbsFunName)name).name;

		System.out.format(
			"%-45s @ %s%n",

			String.format(
				"%su %-7s %-10s",
				debug_prefix(),
				kind, hrname
			),

			name.location()
		);
	}

	private void dbgFunUsageIn(String kind, AbsFunDecl decl) {
		System.out.format(
			"%-45s @ %s%n",

			String.format(
				"%su %-7s %-10s",
				debug_prefix(),
				kind, decl.name
			),

			decl.type.location()
		);
		System.out.format(
			"%s( %n",
			debug_prefix()
		);
		debug_depth++;
	}

	private void dbgFunUsageOut() {
		debug_depth--;
		System.out.format(
			"%s) %n",
			debug_prefix()
		);
	}

	private void dbgScopeIn(
		String kind, String scopeName, Locatable scopeLocation
	) {
		System.out.format(
			"%-45s @ %s%n",

			String.format(
				"%ss %s %s",
				debug_prefix(),
				kind,
				scopeName
			),

			scopeLocation
		);
		System.out.format(
			"%s{ %n",
			debug_prefix()
		);
		debug_depth++;
	}

	private void dbgScopeOut() {
		debug_depth--;
		System.out.format(
			"%s} %n",
			debug_prefix()
		);
	}

	// Since the PREV language specification states that
	// 	All names declared within a given scope are visible in the
	// 	entire scope unless hidden by a declaration in the nested scope.
	//
	// we have to, for each scope independently, first find the declarations
	// and only then check for their usages.
	// 
	// We also have to be aware that
	// the
	//	- name of a function
	//	- the types of parameters and
	//	- the type of a result
	// belong to the (outer) scope of the function declaration 
	//
	// while the
	//	- names of parameters and
	// 	- the expression denoting the function body (if present)
	// belong to the new (inner) scope created by the function declaration.
	//
	//
	//
	// Declarations in outer scope might also be used in the inner scope.
	//
	// The algorithm of traversal is as follows:
	// 1. for current scope, find all declarations
	//    not traversing into deeper scopes (DECLS)
	// 2. for current scope, find all definitions
	//	  not traversing into deeper scopes (USAGES)
	// 3. when the symbol table for the current scope is populated & checked
	//    go into deeper scope (DEEPER)
	//

	// One instance of traversers per Void
	//
	Decls decls = new Decls();
	Usages usages = new Usages();
	Scope scopes = new Scope();

	class Decls extends AbsFullVisitor<Void, Void> {
		// Type declaration
		//
		// This also includes record types.
		//
		// At the name resolution stage, we only add the record type itself into the
		// symbol table. We don't resolve record components here - we will do this
		// during the type checking phase.
		//
		@Override
		public Void visit(AbsTypDecl typDecl, Void visArg) {
			if(debug)
				dbgDecl("type", typDecl);

			typDecl.type.accept(this, visArg);
			try {symbTable.ins(typDecl.name, typDecl);}
			catch (CannotInsNameException __)
			{throw new Report.Error(typDecl, String.format("Type %s already declared", typDecl.name));}
			return null;
		}

		// Record type definition 
		//
		// Record type components are skipped during this phase.
		// We will evaluate them during type checking.
		//
		@Override
		public Void visit(AbsRecType recType, Void visArg) {
			// The actual record type declaration is handled by the typDecl
			// visitor. Therefore no-op here.
			return null;
		}
		@Override
		public Void visit(AbsCompDecls compDecls, Void visArg) {
			return null;
		}
		@Override
		public Void visit(AbsCompDecl compDecl, Void visArg) {
			return null;
		}

		// Variable declaration
		@Override
		public Void visit(AbsVarDecl varDecl, Void visArg) {
			if(debug)
				dbgDecl("var", varDecl);

			varDecl.type.accept(this, visArg);
			try {symbTable.ins(varDecl.name, varDecl);}
			catch (CannotInsNameException __)
			{throw new Report.Error(varDecl, String.format("Variable %s already declared", varDecl.name));}
			return null;
		}

		// Function declaration (name declaration only)
		@Override
		public Void visit(AbsFunDecl funDecl, Void visArg) {
			if(debug)
				dbgDecl("fundec", funDecl);

			try {symbTable.ins(funDecl.name, funDecl);}
			catch (CannotInsNameException __)
			{throw new Report.Error(funDecl, String.format("Function %s already declared", funDecl.name));}

			// Parameters & Expression handled in inner scope, types are only used.

			return null;
		}

		// Function definition (name declaration only)
		@Override
		public Void visit(AbsFunDef funDef, Void visArg) {
			if(debug)
				dbgDecl("fundef", funDef);

			try {symbTable.ins(funDef.name, funDef);}
			catch (CannotInsNameException __)
			{throw new Report.Error(funDef, String.format("Function %s already declared", funDef.name));}

			return null;
		}

		// Function parameters (parameter declaration)
		//
		// We explicitly call the AbsParDecls visitor from within Scopes,
		// within the new scope.
		//
		@Override
		public Void visit(AbsParDecl parDecl, Void visArg) {
			if(debug)
				dbgDecl("argvar", parDecl);
	
			try {symbTable.ins(parDecl.name, parDecl);}
			catch (CannotInsNameException __)
			{throw new Report.Error(parDecl, String.format("Parameter %s already declared", parDecl.name));}

			return null;
		}

		// Usages do not get traversed here (no-ops)
		// Type usage
		@Override
		public Void visit(AbsTypName typName, Void visArg) {
			return null;
		}
		// Variable usage
		@Override
		public Void visit(AbsVarName varName, Void visArg) {
			return null;
		}
		// Function usage (call)
		@Override
		public Void visit(AbsFunName funName, Void visArg) {
			return null;
		}

		// Scopes do not get traversed here (no-ops)
		// Compound expression (inner scope)
		@Override
		public Void visit(AbsBlockExpr blockExpr, Void visArg) {
			return null;
		}
	}

	class Usages extends AbsFullVisitor<Void, Void> {

		// Function declaration (parameter & return type usage check)
		@Override
		public Void visit(AbsFunDecl funDecl, Void visArg) {
			if(debug)
				dbgFunUsageIn("fundec", funDecl);

			// Check usage of types.
			funDecl.parDecls.accept(this, visArg);
			// Check usage of return type.
			funDecl.type.accept(this, visArg);

			if(debug)
				dbgFunUsageOut();
			
			return null;
		}

		// Function definition (parameter & return type usage check)
		@Override
		public Void visit(AbsFunDef funDef, Void visArg) {
			if(debug)
				dbgFunUsageIn("fundef", funDef);

			// Check usage of types.
			funDef.parDecls.accept(this, visArg);
			// Check usage of return type.
			funDef.type.accept(this, visArg);

			// Expression is handled within inner scope (Scopes)

			if(debug)
				dbgFunUsageOut();

			return null;
		}

		// Function parameters (parameter type usage check)
		//
		@Override
		public Void visit(AbsParDecl parDecl, Void visArg) {

			// Check usage of types
			parDecl.type.accept(this, visArg);

			return null;
		}

		// Type usage
		@Override
		public Void visit(AbsTypName typName, Void visArg) {
			if(debug)
				dbgUsage("type", typName);

			try {SemAn.declaredAt.put(typName, symbTable.fnd(typName.name));}
			catch (CannotFndNameException __)
			{throw new Report.Error(typName, String.format("Type %s not defined", typName.name));}
			return null;
		}

		// Variable usage
		@Override
		public Void visit(AbsVarName varName, Void visArg) {
			if(debug)
				dbgUsage("var", varName);

			try {SemAn.declaredAt.put(varName, symbTable.fnd(varName.name));}
			catch (CannotFndNameException __)
			{throw new Report.Error(varName, String.format("Variable %s not defined", varName.name));}
			return null;
		}

		// Record usage
		@Override
		public Void visit(AbsRecExpr recExpr, Void visArg) {
			recExpr.record.accept(this, visArg);
			
			// Record type components are skipped during this phase.
			// We will evaluate them during type checking.
			return null;
		}

		// Function usage (call)
		@Override
		public Void visit(AbsFunName funName, Void visArg) {
			if(debug)
				dbgUsage("fun", funName);

			try {SemAn.declaredAt.put(funName, symbTable.fnd(funName.name));}
			catch (CannotFndNameException __)
			{throw new Report.Error(funName, String.format("Function %s not defined", funName.name));}

			funName.args.accept(this, visArg);
			return null;
		}

		// Scopes do not get traversed here
		// Compound expression (inner scope)
		@Override
		public Void visit(AbsBlockExpr blockExpr, Void visArg) {
			return null;
		}
	}

	// Enters lower scopes
	// 
	class Scope extends AbsFullVisitor<Void, Void> {

		// Compound expression (inner scope)
		@Override
		public Void visit(AbsBlockExpr blockExpr, Void visArg) {
			if(debug)
				dbgScopeIn("blockexpr", "", blockExpr);

			// Compound expression body in new scope
			symbTable.newScope();
			
			blockExpr.decls.accept(decls, visArg);
			blockExpr.decls.accept(usages, visArg);
			blockExpr.decls.accept(scopes, visArg);

			blockExpr.stmts.accept(usages, visArg);
			blockExpr.stmts.accept(scopes, visArg);

			blockExpr.expr.accept(usages, visArg);
			blockExpr.expr.accept(scopes, visArg);

			symbTable.oldScope();

			if(debug)
				dbgScopeOut();

			return null;
		}

		// Function definition (inner scope)
		@Override
		public Void visit(AbsFunDef funDef, Void visArg) {
			if(debug)
				dbgScopeIn("fundef", funDef.name, funDef);

			// Expression denoting the function body in new scope
			symbTable.newScope();

			// Declare function parameters in inner scope
			funDef.parDecls.accept(decls, visArg);
			
			// decl-usage-scope for function body in inner scope
			funDef.value.accept(decls, visArg);
			funDef.value.accept(usages, visArg);
			funDef.value.accept(scopes, visArg);
			
			symbTable.oldScope();

			if(debug)
				dbgScopeOut();

			return null;
		}
	}

	// This starts the whole decls-usages-deeper traversal
	//
	@Override
	public Void visit(AbsSource source, Void visArg) {
		source.decls.accept(decls, visArg);
		source.decls.accept(usages, visArg);
		source.decls.accept(scopes, visArg);
		return null;
	}
}
