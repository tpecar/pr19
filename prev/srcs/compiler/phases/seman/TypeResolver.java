/**
 * @author sliva 
 */
package compiler.phases.seman;

import java.util.*;

import compiler.common.report.*;
import compiler.data.abstree.*;
import compiler.data.abstree.visitor.*;
import compiler.data.type.*;
import compiler.phases.seman.SymbTable.CannotFndNameException;
import compiler.phases.seman.SymbTable.CannotInsNameException;

/**
 * Type resolving: the Void is stored in {@link SemAn#declaresType},
 * {@link SemAn#isType}, and {@link SemAn#ofType}.
 * 
 * The original implementation squeezed all logic into the same visitor, with
 * the TypeResolver.Phase determining which phase of type resolving we're in.
 * 
 * We will go for an alternative approach of having a separate visitor class
 * for each phase. The TypeResolver will only implement the Source visitor,
 * which will run the phase visitors.
 * 
 * @author sliva & TP
 */
public class TypeResolver extends AbsNullVisitor<SemType, Object> {

	/** Symbol tables of individual record types. */
	public static final HashMap<SemRecType, SymbTable> symbTables = new HashMap<SemRecType, SymbTable>();

	////////////////////////////////////////////////////////////////////////////
	//
	//	Additional note on visitor return values:
	//	Most of the SemXType classes implement interfaces which determine their
	//	context.
	//	(for eg., the SemBoolType implements AssignType, BinEquOperType, etc.)
	//
	//	So, instead of returning/accepting the specific SemXType object,
	//	we will return it as a context class object.
	//
	//	This way, the java RTTI will help us find any issues, where an
	//	inappropriate type would be returned.
	//
	////////////////////////////////////////////////////////////////////////////

	BIND 	bind 	= new BIND();
	ISTYPE	isType 	= new ISTYPE();
	OFTYPE	ofType 	= new OFTYPE();

	////////////////////////////////////////////////////////////////////////////
	// Stage 1 - BIND
	// Type declaration to a {@link SemNamedType} object map.
	//

	// This identifier-declaration map is defined by the BIND function,
	// located in the expression _above_ the line
	// (the antencendent of the implication).
	//
	// Later maps use the expression _under_ the line
	// (the consequent of the implication).
	
	class BIND extends AbsFullVisitor<Void, Void> {
	
	// ------------------------------------------------------------------------
	// Declarations.

	// (D1)	[[identifier ]]BIND = typ identifier :type [[type]]ISTYPE = τ
	// 		-------------------------------------------------------------
	// 		[[identifier ]]ISTYPE = τ
	
	// This encompases all _data_ type definitions
	// (primitive types, arrays, records).
	//
	// Function types are handled separately.
	//
	// At BIND stage, we determine all type declarations, but don't map to the actual types yet.
	// At ISTYPE stage, we
	//		bind all declarations to their types.
	//		bind all type usages to their declarations.

	@Override
	public Void visit(AbsTypDecl typDecl, Void visArg) {
		SemNamedType type;

		// Type declarations are leaf nodes, no need to recurse
		type = new SemNamedType(typDecl.name);

		SemAn.declaresType.put(typDecl, type);
		return null;
	}

	// (D2)	[[identifier ]]BIND = var identifier :type [[type]]ISTYPE = τ
	// 		τ ∈ Td \ {void}
	// 		-------------------------------------------------------------
	// 		[[identifier ]]OFTYPE = τ
	
	// Variable declaration statement does not declare any named type -
	// it only binds the variable name to its type, which might not have been
	// resolved yet at this stage.
	//
	// We bind it to the actual type in the ISTYPE stage.

	// (D3)	[[identifier ]]BIND =
	// 			fun identifier (identifer 1 :type 1 , . . . ,identifer n :type n ):type
	// 		[[type 1 ]]ISTYPE = τ1 . . . [[type n ]]ISTYPE = τn
	// 		[[type]]ISTYPE = τ
	// 		∀i ∈ {1 . . . n}: τi ∈ {bool, char, int} ∪ {ptr(τ ) | τ ∈ Td }
	// 		τ ∈ {void, bool, char, int} ∪ {ptr(τ ) | τ ∈ Td }
	// 		-------------------------------------------------------------------
	// 		[[identifier ]]OFTYPE = (τ1 , . . . , τn ) → τ
	
	// Function declaration / definition statement does not declare any type -
	// it only binds the function name to the type of its return value -
	// since we also need to check the validity of arguments before the binding is
	// made, we evaluate this in OFTYPE stage.

	} // BIND

	////////////////////////////////////////////////////////////////////////////
	// Stage 2 - ISTYPE
	// Type expression to type map.
	//
	// DO NOTE that during the ISTYPE phase, the type declarations might not be
	// resolved during traversal, due to the possibility of using a type before
	// its declaration.
	//
	// Therefore, when using type declarations, we are only binding the
	// declaration (alias) here.
	//
	// This also implies that actual type check cannot be done within this phase.

	class ISTYPE extends AbsFullVisitor<Void, Void> {
	
	Report.Error error(Locatable l, String s) { return new Report.Error(l, "ISTYPE: " + s); }

	// -------------------------------------------------------------------------
	// Type expressions.

	// (T1)	---------------------  ---------------------  ---------------------
	// 		[[void]]ISTYPE = void  [[bool]]ISTYPE = bool  [[char]]ISTYPE = char
	//
	// 		-------------------
	// 		[[int]]ISTYPE = int

	@Override
	public Void visit(AbsAtomType atomType, Void visArg) {
		SemType type;

		switch(atomType.type) {
			case VOID: type = new SemVoidType(); break;
			case BOOL: type = new SemBoolType(); break;
			case CHAR: type = new SemCharType(); break;
			case INT : type = new SemIntType();  break;
			
			default: throw new Report.InternalError();
		}

		SemAn.isType.put(atomType, type);
		return null;
	}

	// (T2)	[[type]]ISTYPE = τ val(int) = n
	// 		n > 0 τ ∈ Td \ {void}
	// 		------------------------------------
	// 		[[arr[int]type]]ISTYPE = arr(n × τ )
	
	@Override
	public Void visit(AbsArrType arrType, Void visArg) {
		SemType type;

		long len = 0;

		{
			// arrType.len.accept(this, visArg);
			//
			// We could recursively parse expressions here, but for the sake of
			// simplicity, we only allow an integer atomic here
			//
			if(!(arrType.len instanceof AbsAtomExpr))
				throw error(arrType.len, "Expected atomic expression");
			AbsAtomExpr lenExpr = ((AbsAtomExpr)arrType.len);

			try {
				len = (lenExpr.type == AbsAtomExpr.Type.INT) ?
				Long.parseLong(lenExpr.expr) : 0;
			}
			catch(NumberFormatException e){}

			if(len < 1)
				throw error(arrType.len, "Expected positive integer");
			
			SemAn.ofType.put(arrType.len, new SemIntType());
		}
		arrType.elemType.accept(this, visArg);

		type = new SemArrType(len, SemAn.isType.getExisting(arrType.elemType));

		SemAn.isType.put(arrType, type);
		return null;
	}

	// (T3)	[[type 1 ]]ISTYPE = τ1 . . . [[type n ]]ISTYPE = τn
	// 		∀i ∈ {1 . . . n}: τi ∈ Td \ {void}
	// 		---------------------------------------------------
	// 		[[rec(id 1 :type 1 , . . . ,id n :type n )]]ISTYPE
	// 			= recid 1 ,...,id n (τ1 , . . . , τn )

	@Override
	public Void visit(AbsRecType recType, Void visArg) {
		SemRecType type;

		SymbTable recSymbTable = new SymbTable();

		// recType.compDecls.accept(this, visArg);
		// Parse compDecls in this visitor, since they are bound to the
		// record symbol table
		//
		Vector<SemType> compTypes = new Vector<>();
		for (AbsCompDecl compDecl : recType.compDecls.compDecls()) {
			// resolve type for the member
			compDecl.accept(this, visArg);
			compTypes.add(SemAn.isType.getExisting(compDecl.type));

			// add to symbol table, used @ OFTYPE
			try {recSymbTable.ins(compDecl.name, compDecl);}
			catch(CannotInsNameException e)
			{throw error(compDecl, "Duplicate record member");}
		}
		type = new SemRecType(compTypes);

		// bind symbol table to record, used @ OFTYPE
		symbTables.put(type, recSymbTable);

		SemAn.isType.put(recType, type);
		return null;
	}

	// (T4)	[[type]]ISTYPE = τ τ ∈ Td
	// 		----------------------------
	// 		[[ptr type]]ISTYPE = ptr(τ )
	
	@Override
	public Void visit(AbsPtrType ptrType, Void visArg) {
		SemType type;

		ptrType.ptdType.accept(this, visArg);

		type = new SemPtrType(SemAn.isType.getExisting(ptrType.ptdType));

		SemAn.isType.put(ptrType, type);
		return null;
	}

	// (T5)	[[type]]ISTYPE = τ
	// 		--------------------
	// 		[[(type)]]ISTYPE = τ

	// SynAn phase discards parentheses, so we don't need to handle
	// enclosures at AST level.

	// ------------------------------------------------------------------------
	// Declarations.

	// (D1)	[[identifier ]]BIND = typ identifier :type [[type]]ISTYPE = τ
	// 		-------------------------------------------------------------
	// 		[[identifier ]]ISTYPE = τ
	//

	// Resolve the declaration

	@Override
	public Void visit(AbsTypDecl typDecl, Void visArg) {

		// Resolve the type that the declaration aliases
		typDecl.type.accept(this, visArg); SemType subType = SemAn.isType.getExisting(typDecl.type);
		SemAn.declaresType.get(typDecl).define(subType);

		return null;
	}

	// Bind type usage to its declaration.

	@Override
	public Void visit(AbsTypName typName, Void visArg) {
		SemType type;

		AbsDecl decl = SemAn.declaredAt.getExisting(typName);
		if(!(decl instanceof AbsTypDecl))
			throw error(typName, typName + " does not refer to a type declaration.");

		type = SemAn.declaresType.getExisting((AbsTypDecl) decl);

		SemAn.isType.put(typName, type);
		return null;
	}


	} // ISTYPE

	////////////////////////////////////////////////////////////////////////////
	// Stage 3 - OFTYPE
	// Value expression to type map.

	class OFTYPE extends AbsFullVisitor<Void, Void> {

	Report.Error error(Locatable l, String s) { return new Report.Error(l, "OFTYPE: " + s); }

	Report.Error error(Locatable l, String s, SemType t) {
		return new Report.Error(
			l, String.format("OFTYPE: Expected %s got %s", s, t.getClass().getSimpleName())
		); 
	}

	Report.Error error(Locatable l, String s, SemType t1, SemType t2) {
		return new Report.Error(
			l, String.format(
				"OFTYPE: Expected %s got (%s %s)", s, 
				t1.getClass().getSimpleName(), t2.getClass().getSimpleName()
			)
		); 
	}

	// Validity of function arguments and return value type are done at both
	// at function call as at declaration / definition.
	//
	// This is so, because:
	// 	- calling a function before its declaration is allowed - not checking in this
	//    situation would allow erronous types to propagate higher in the AST
	//
	//  - declaring a function without explicitly calling it could otherwise allow
	//    non-valid function definition, which is still compiled in further stages
	//
	void checkFunTypes(AbsParDecls absParDecls, AbsType absType) {
		absParDecls.parDecls().stream().forEach(
			parDecl -> {
				SemType type = SemAn.isType.getExisting(parDecl.type).actualType();
				if(!(
					type instanceof SemBoolType ||
					type instanceof SemCharType ||
					type instanceof SemIntType ||
					type instanceof SemPtrType
				))
					throw error(parDecl, "τi ∈ {bool, char, int} ∪ {ptr(τ ) | τ ∈ Td } for argument type τi", type);
			}
		);

		SemType type = SemAn.isType.getExisting(absType).actualType();
		if(!(
			type instanceof SemVoidType ||
			type instanceof SemBoolType ||
			type instanceof SemCharType ||
			type instanceof SemIntType ||
			type instanceof SemPtrType
		))
			throw error(absType, "τ ∈ {void, bool, char, int} ∪ {ptr(τ ) | τ ∈ Td } for return value type τ", type);
	}

	// ------------------------------------------------------------------------
	// Value expressions.

	@Override
	public Void visit(AbsAtomExpr atomExpr, Void visArg) {
		SemType type;
	
	// (V1)	---------------------	--------------------------
	// 		[[none]]OFTYPE = void	[[null]]OFTYPE = ptr(void)
	//
	// 		----------------------------
	// 		[[string]]OFTYPE = ptr(char)

	// (V2)	----------------------	----------------------	-------------------
	// 		[[bool ]]OFTYPE = bool	[[char ]]OFTYPE = char	[[int]]OFTYPE = int

		switch(atomExpr.type) {
			case VOID: type = new SemVoidType(); break;
			case BOOL: type = new SemBoolType(); break;
			case CHAR: type = new SemCharType(); break;
			case INT : type = new SemIntType(); break;
			case PTR : type = new SemPtrType(new SemVoidType()); break;	// PTRCONST (null)
			case STR : type = new SemPtrType(new SemCharType()); break;	// STRCONST

			default: throw new Report.InternalError();
		}

		SemAn.ofType.put(atomExpr, type);
		return null;
	}


	@Override
	public Void visit(AbsBinExpr binExpr, Void visArg) {
		SemType type;

	// (V4)	[[expr 1 ]]OFTYPE = bool [[expr 2 ]]OFTYPE = bool op ∈ {&, |, ^}
	// 		----------------------------------------------------------------
	// 		[[expr 1 op expr 2 ]]OFTYPE = bool

	// (V5)	[[expr 1 ]]OFTYPE = τ [[expr 2 ]]OFTYPE = τ
	// 		τ ∈ {char, int} op ∈ {+, -, *, /, %}
	// 		---------------------------------
	// 		[[expr 1 op expr 2 ]]OFTYPE = int

	// (V6)	[[expr 1 ]]OFTYPE = τ [[expr 2 ]]OFTYPE = τ
	// 		τ ∈ {bool, char, int} ∪ {ptr(τ ) | τ ∈ Td } op ∈ {==, !=}
	// 		---------------------------------------------------------
	// 		[[expr 1 op expr 2 ]]OFTYPE = bool

	// (V7)	[[expr 1 ]]OFTYPE = τ [[expr 2 ]]OFTYPE = τ
	// 		τ ∈ {char, int} ∪ {ptr(τ ) | τ ∈ Td } op ∈ {<=, >=, <, >}
	// 		---------------------------------------------------------
	// 		[[expr 1 op expr 2 ]]OFTYPE = bool

		// Resolve operand types first
		binExpr.fstExpr.accept(this, visArg); SemType fstExpr = SemAn.ofType.getExisting(binExpr.fstExpr).actualType();
		binExpr.sndExpr.accept(this, visArg); SemType sndExpr = SemAn.ofType.getExisting(binExpr.sndExpr).actualType();

		switch(binExpr.oper) {
			case AND: case IOR: case XOR: {
				if
				(
					fstExpr instanceof SemBoolType &&
					sndExpr instanceof SemBoolType
				)
					type = new SemBoolType();
				else
					throw error(binExpr, "[[expr 1 ]]OFTYPE = bool [[expr 2 ]]OFTYPE = bool op ∈ {&, |, ^}", fstExpr, sndExpr);
				break;
			}
			case ADD: case SUB: case MUL: case DIV: case MOD: {
				if
				(
					(fstExpr instanceof SemCharType || fstExpr instanceof SemIntType) &&
					(sndExpr instanceof SemCharType || sndExpr instanceof SemIntType)
				)
					type = new SemIntType();
				else
					throw error(binExpr, "[[expr 1 ]]OFTYPE = τ [[expr 2 ]]OFTYPE = τ  τ ∈ {char, int} op ∈ {+, -, *, /, %}", fstExpr, sndExpr);
				break;
			}
			case EQU: case NEQ: {
				if
				(
					(fstExpr instanceof SemBoolType || fstExpr instanceof SemCharType || fstExpr instanceof SemIntType) &&
					(sndExpr instanceof SemBoolType || sndExpr instanceof SemCharType || sndExpr instanceof SemIntType)
					
					// The {ptr(τ ) | τ ∈ Td } asertion is implicitly satisfied by successfully
					// retrieving the evaluated type via OFTYPE
				)
					type = new SemBoolType();
				else
					throw error(binExpr, "[[expr 1 ]]OFTYPE = τ [[expr 2 ]]OFTYPE = τ  τ ∈ {bool, char, int} ∪ {ptr(τ ) | τ ∈ Td } op ∈ {==, !=}", fstExpr, sndExpr);
				break;
			}
			case LEQ: case GEQ: case LTH: case GTH: {
				if
				(
					(fstExpr instanceof SemCharType || fstExpr instanceof SemIntType) &&
					(sndExpr instanceof SemCharType || sndExpr instanceof SemIntType)
				)
					type = new SemBoolType();
				else
					throw error(binExpr, "[[expr 1 ]]OFTYPE = τ [[expr 2 ]]OFTYPE = τ  τ ∈ {char, int} ∪ {ptr(τ ) | τ ∈ Td } op ∈ {<=, >=, <, >}", fstExpr, sndExpr);
				break;
			}

			default: throw new Report.InternalError();
		}

		SemAn.ofType.put(binExpr, type);
		return null;
	}

	@Override
	public Void visit(AbsUnExpr unExpr, Void visArg) {
		SemType type;

	// (V3)	[[expr ]]OFTYPE = bool		[[expr ]]OFTYPE = int op ∈ {+, -}
	// 		------------------------	---------------------------------
	// 		[[! expr ]]OFTYPE = bool	[[op expr ]]OFTYPE = int

	// (V8)	[[expr ]]OFTYPE = τ τ ∈ Td \ {void}
	// 		-----------------------------------
	// 		[[$ expr ]]OFTYPE = ptr(τ )

	// 		[[expr ]]OFTYPE = ptr(τ ) τ ∈ Td \ {void}
	// 		-----------------------------------------
	// 		[[@ expr ]]OFTYPE = τ

		// Resolve operand type first
		unExpr.subExpr.accept(this, visArg); SemType subExpr = SemAn.ofType.getExisting(unExpr.subExpr).actualType();

		switch(unExpr.oper) {
			case NOT: {
				if(subExpr instanceof SemBoolType)
					type = new SemBoolType();
				else
					throw error(unExpr, "[[expr ]]OFTYPE = bool", subExpr);
				break;
			}
			case ADD: case SUB: {
				if(subExpr instanceof SemIntType)
					type = new SemIntType();
				else
					throw error(unExpr, "[[expr ]]OFTYPE = int op ∈ {+, -}", subExpr);
				break;
			}
			case ADDR: {
				// The {ptr(τ ) | τ ∈ Td } asertion is implicitly satisfied by successfully
				// retrieving the evaluated type via OFTYPE

				if(subExpr instanceof SemVoidType)
					throw error(unExpr, "[[expr ]]OFTYPE = τ τ ∈ Td \\ {void}", subExpr);

				type = new SemPtrType(subExpr);
				break;
			}
			case DATA: {
				if(subExpr instanceof SemPtrType) {
					type = ((SemPtrType)subExpr).ptdType;
					if(type instanceof SemVoidType)
						throw error(unExpr, "[[expr ]]OFTYPE = ptr(τ ) τ ∈ Td \\ {void}", subExpr);
				}
				else
					throw error(unExpr, "[[expr ]]OFTYPE = ptr(τ )", subExpr);
				break;
			}
			
			default: throw new Report.InternalError();
		}

		SemAn.ofType.put(unExpr, type);
		return null;
	}

	// (V9)	[[type]]ISTYPE = τ τ ∈ Td \ {void}
	// 		----------------------------------
	// 		[[new(type)]]OFTYPE = ptr(τ )

	@Override
	public Void visit(AbsNewExpr newExpr, Void visArg) {
		SemType type;

		// Enclosing type has already been resolved during the ISTYPE phase
		SemType subType = SemAn.isType.getExisting(newExpr.type).actualType();

		if(subType instanceof SemVoidType)
			throw error(newExpr, "[[type]]ISTYPE = τ τ ∈ Td \\ {void}", subType);

		type = new SemPtrType(subType);

		SemAn.ofType.put(newExpr, type);
		return null;
	}

	// 		[[expr ]]OFTYPE = ptr(τ ) τ ∈ Td \ {void}
	// 		-----------------------------------------
	// 		[[del(expr )]]OFTYPE = void

	@Override
	public Void visit(AbsDelExpr delExpr, Void visArg) {
		SemType type;

		// Resolve subexpression type
		delExpr.expr.accept(this, visArg); SemType expr = SemAn.ofType.getExisting(delExpr.expr).actualType();

		if(expr instanceof SemVoidType)
			throw error(delExpr, "[[expr ]]OFTYPE = ptr(τ ) τ ∈ Td \\ {void}", expr);
		
		type = new SemVoidType();
		
		SemAn.ofType.put(delExpr, type);
		return null;
	}

	// (V10)	[[expr 1 ]]OFTYPE = arr(n × τ ) [[expr 2 ]]OFTYPE = int
	// 			-------------------------------------------------------
	// 			[[expr 1 [expr 2 ]]]OFTYPE = τ

	@Override
	public Void visit(AbsArrExpr arrExpr, Void visArg) {
		SemType type;

		arrExpr.array.accept(this, visArg); SemType array = SemAn.ofType.getExisting(arrExpr.array).actualType();
		arrExpr.index.accept(this, visArg); SemType index = SemAn.ofType.getExisting(arrExpr.index).actualType();

		if(!(array instanceof SemArrType))
			throw error(arrExpr.array, "array: [[expr 1 ]]OFTYPE = arr(n × τ )", array);

		if(!(index instanceof SemIntType))
			throw error(arrExpr.index, "array index: [[expr 2 ]]OFTYPE = int", index);
		
		type = ((SemArrType)array).elemType;

		SemAn.ofType.put(arrExpr, type);
		return null;
	}

	// (V11)	[[expr ]]OFTYPE = recid 1 ,...,id n (τ1 , . . . , τn )
	// 			identifier = id i
	// 			------------------------------------------------------
	// 			[[expr .identifier ]]OFTYPE = τi

	@Override
	public Void visit(AbsRecExpr recExpr, Void visArg) {
		SemType type;

		// Resolve type of the record expression
		recExpr.record.accept(this, visArg); SemType record = SemAn.ofType.getExisting(recExpr.record).actualType();
		
		if(!(record instanceof SemRecType))
			throw error(recExpr, "record: [[expr ]]OFTYPE = recid 1 ,...,id n (τ1 , . . . , τn )", record);
		
		try
		{
			type = 
				// get SemType for the accessed member's AbsType object
				SemAn.isType.getExisting(
					// get SymbTable for the record object
					symbTables.get((SemRecType)record)
						// get member object through the name of the accessed member
						.fnd(recExpr.comp.name).type
				).actualType();
		}
		catch (CannotFndNameException e) {throw new Report.InternalError();}

		SemAn.ofType.put(recExpr, type);
		return null;
	}

	// (V12)	[[identifier ]]OFTYPE = (τ1 , . . . , τn ) → τ
	// 			[[expr 1 ]]OFTYPE = τ1 . . . [[expr n ]]OFTYPE = τn
	// 			∀i ∈ {1 . . . n}: τi ∈ {bool, char, int} ∪ {ptr(τ ) | τ ∈ Td }
	// 			τ ∈ {void, bool, char, int} ∪ {ptr(τ ) | τ ∈ Td }
	// 			--------------------------------------------------------------
	// 			[[identifier (expr 1 , . . . ,expr n )]]OFTYPE = τ

	@Override
	public Void visit(AbsFunName funName, Void visArg) {
		SemType type;

		// Resolve function call arguments
		funName.args.accept(this, visArg);

		// Get function declaration and check for type equality
		AbsDecl decl = SemAn.declaredAt.getExisting(funName);
		if(!(decl instanceof AbsFunDecl))
			throw error(funName, "Name does not refer to a function definition");
		
		AbsFunDecl funDecl = (AbsFunDecl)decl;

		// Check if the types in function declaration are valid
		checkFunTypes(funDecl.parDecls, funDecl.type);

		// Check if function argument types match
		if(funDecl.parDecls.numParDecls() != funName.args.numArgs())
			throw error(funName,
				String.format(
					"Number of arguments mismatch between call (%d) and definition (%d) @ %s",
					funName.args.numArgs(),
					funDecl.parDecls.numParDecls(),
					funDecl
				));

		for(int pidx = 0; pidx < funDecl.parDecls.numParDecls(); pidx++) {
			SemType declType = SemAn.isType.getExisting(funDecl.parDecls.parDecl(pidx).type).actualType();
			SemType nameType = SemAn.ofType.getExisting(funName.args.arg(pidx)).actualType();

			if(!declType.matches(nameType))
				throw error(funName.args.arg(pidx),
					String.format("Argument type mismatch between call (%s) and definition (%s)",
						nameType.toString(),
						declType.toString()
					));
		}

		type = SemAn.isType.getExisting(funDecl.type).actualType();
		
		SemAn.ofType.put(funName, type);
		return null;
	}

	// (V13)	[[expr ]]OFTYPE = τ
	// 			--------------------------------------
	// 			[[{stmts:expr where decls}]]OFTYPE = τ

	@Override
	public Void visit(AbsBlockExpr blockExpr, Void visArg) {
		SemType type;

		blockExpr.decls.accept(this, visArg);
		blockExpr.stmts.accept(this, visArg);
		blockExpr.expr.accept(this, visArg);

		type = SemAn.ofType.getExisting(blockExpr.expr).actualType();

		SemAn.ofType.put(blockExpr, type);
		return null;
	}

	// (V14)	[[expr ]]OFTYPE = τ1 [[type]]ISTYPE = τ2
	// 			τ1 , τ2 ∈ {char, int} ∪ {ptr(τ ) | τ ∈ Td }
	// 			-------------------------------------------
	// 			[[(expr :type)]]OFTYPE = τ2

	@Override
	public Void visit(AbsCastExpr castExpr, Void visArg) {
		SemType type;

		castExpr.type.accept(this, visArg); SemType castType = SemAn.isType.getExisting(castExpr.type).actualType();
		castExpr.expr.accept(this, visArg);	SemType expr = SemAn.ofType.getExisting(castExpr.expr).actualType();

		if(!(
			(expr 		instanceof SemCharType || expr 		instanceof SemIntType || expr 		instanceof SemPtrType) &&
			(castType 	instanceof SemCharType || castType 	instanceof SemIntType || castType 	instanceof SemPtrType)
		))
			throw error(castExpr, "[[expr ]]OFTYPE = τ1 [[type]]ISTYPE = τ2  τ1 , τ2 ∈ {char, int} ∪ {ptr(τ ) | τ ∈ Td }", expr, castType);

		type = castType;

		SemAn.ofType.put(castExpr, type);
		return null;
	}

	// (V15)	[[expr ]]OFTYPE = τ
	// 			---------------------
	// 			[[(expr )]]OFTYPE = τ

	// SynAn phase discards parentheses, so we don't need to handle
	// enclosures at AST level.

	// ------------------------------------------------------------------------
	// Declarations.

	// (D2)	[[identifier ]]BIND = var identifier :type [[type]]ISTYPE = τ
	// 		τ ∈ Td \ {void}
	// 		-------------------------------------------------------------
	// 		[[identifier ]]OFTYPE = τ

	// Same reasoning as in BIND. Here we only check the validity of the type used
	// for the variable.

	@Override
	public Void visit(AbsVarDecl varDecl, Void visArg) {
		// Resolve underlying type
		varDecl.type.accept(this, visArg); SemType type = SemAn.isType.getExisting(varDecl.type).actualType();

		if(type instanceof SemVoidType)
			throw error(varDecl, "Expected non-void type.");
		
		return null;
	}

	// Bind variable usage to its type

	@Override
	public Void visit(AbsVarName varName, Void visArg) {
		SemType type;

		// Variable declaration has been checked during ISTYPE stage
		type = SemAn.isType.getExisting(SemAn.declaredAt.getExisting(varName).type).actualType();

		SemAn.ofType.put(varName, type);
		return null;
	}

	// (D3)	[[identifier ]]BIND =
	// 			fun identifier (identifer 1 :type 1 , . . . ,identifer n :type n ):type
	// 		[[type 1 ]]ISTYPE = τ1 . . . [[type n ]]ISTYPE = τn
	// 		[[type]]ISTYPE = τ
	// 		∀i ∈ {1 . . . n}: τi ∈ {bool, char, int} ∪ {ptr(τ ) | τ ∈ Td }
	// 		τ ∈ {void, bool, char, int} ∪ {ptr(τ ) | τ ∈ Td }
	// 		-------------------------------------------------------------------
	// 		[[identifier ]]OFTYPE = (τ1 , . . . , τn ) → τ

	// Same reasoning as in BIND. Here we only check the validity of the type used
	// for the return value.

	@Override
	public Void visit(AbsFunDecl funDecl, Void visArg) {
		// Resolve underlying types
		funDecl.parDecls.accept(this, visArg);
		funDecl.type.accept(this, visArg);

		checkFunTypes(funDecl.parDecls, funDecl.type);

		return null;
	}

	@Override
	public Void visit(AbsFunDef funDef, Void visArg) {
		funDef.parDecls.accept(this, visArg);
		funDef.type.accept(this, visArg);
		checkFunTypes(funDef.parDecls, funDef.type);

		funDef.value.accept(this, visArg);

		return null;
	}


	} // OFTYPE

	@Override
	public SemType visit(AbsSource source, Object visArg) {
		source.decls.accept(bind, null);
		source.decls.accept(isType, null);
		source.decls.accept(ofType, null);
		return null;
	}

}
