/**
 * @author sliva
 */
package compiler.phases.seman;

import compiler.common.report.*;
import compiler.data.abstree.*;
import compiler.data.abstree.visitor.*;
import compiler.data.type.SemPtrType;

/**
 * Determines which value expression can denote an address.
 * 
 * @author sliva
 */
public class AddrResolver extends AbsNullVisitor<Boolean, Object> {

    ////////////////////////////////////////////////////////////////////////////
    // Stages 1 - 3 are defined within the TypeResolver class.

	ISADDR 			isaddr 			= new ISADDR();
	ISADDR_CHECK 	isaddr_check 	= new ISADDR_CHECK();

	////////////////////////////////////////////////////////////////////////////
	// Stage 4 - ISADDR
	// Value expression to addressable status map.
	//
	// Here we tag which expressions can be in the role of an lvalue
	// (can be assigned to).

	// Lvalues
	// ------------------------------------------------------------------------
	
	class ISADDR extends AbsFullVisitor<Void, Void> {

	@Override
	public Void visit(AbsVarName varName, Void visArg) {

	// We handle simple variable accesses here.
	//
	// This means that any data variable, either primitive or composite,
	// is accessed directly, without indexing / component select operations
	// (the latter two are handled in separate visitors).

	// [[identifier ]]BIND = variable declaration
	// ------------------------------------------
	// [[identifier ]]ISADDR = true

	// [[identifier ]]BIND = parameter declaration
	// -------------------------------------------
	// [[identifier ]]ISADDR = true

		AbsDecl decl = SemAn.declaredAt.getExisting(varName);

		SemAn.isAddr.put(
			varName,

			decl instanceof AbsVarDecl || decl instanceof AbsParDecl
		);
	
		return null;
	}

	// [[expr ]]OFTYPE = ptr(τ )
	// -------------------------
	// [[@ expr ]]ISADDR = true

	@Override
	public Void visit(AbsUnExpr unExpr, Void visArg) {
		unExpr.subExpr.accept(this, visArg);

		SemAn.isAddr.put(
			unExpr,
			
			unExpr.oper == AbsUnExpr.Oper.DATA &&
			SemAn.ofType.getExisting(unExpr.subExpr).actualType() instanceof SemPtrType
		);
		
		return null;
	}

	// [[expr ]]ISADDR = true
	// ------------------------------
	// [[expr [expr' ]]]ISADDR = true

	@Override
	public Void visit(AbsArrExpr arrExpr, Void visArg) {
		arrExpr.array.accept(this, visArg);
		// Don't traverse index

		SemAn.isAddr.put(
			arrExpr,

			SemAn.isAddr.getExisting(arrExpr.array)
		);

		return null;
	}

	// [[expr ]]ISADDR = true
	// -----------------------------------
	// [[expr .identifier ]]]ISADDR = true
	
	@Override
	public Void visit(AbsRecExpr recExpr, Void visArg) {
		recExpr.record.accept(this, visArg);
		// Don't traverse identifier

		SemAn.isAddr.put(
			recExpr,

			SemAn.isAddr.getExisting(recExpr.record)
		);

		return null;
	}

	// As per spec
	//   In all other cases the value of [[·]]ISADDR equals false.
	//
	// In the case a node is not in the ISADDR map, we will consider the
	// result to be false.

	}

	////////////////////////////////////////////////////////////////////////////
	// Stage 5 - ISADDR_CHECK
	// Checks if all assignment targets are to lvalues.

	class ISADDR_CHECK extends AbsFullVisitor<Void, Void> {

		@Override
		public Void visit(AbsAssignStmt assignStmt, Void visArg) {
			assignStmt.dst.accept(this, visArg);
			assignStmt.src.accept(this, visArg);

			// Expressions are not added to the isAddr map
			// If an AST node is not part of the isAddr map, we consider it not an lvalue
			Boolean dst = SemAn.isAddr.get(assignStmt.dst);
			if(dst == null || !dst)
				throw new Report.Error(assignStmt, "Assignment dst not an lvalue.");

			return null;
		}

	}

	
	@Override
	public Boolean visit(AbsSource source, Object visArg) {
		source.decls.accept(isaddr, null);
		source.decls.accept(isaddr_check, null);
		return null;
	}
}
