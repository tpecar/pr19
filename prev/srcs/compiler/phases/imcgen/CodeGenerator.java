/**
 * @author sliva
 */
package compiler.phases.imcgen;

import static compiler.data.imcode.ImcBINOP.Oper.ADD;
import static compiler.data.imcode.ImcBINOP.Oper.AND;
import static compiler.data.imcode.ImcBINOP.Oper.DIV;
import static compiler.data.imcode.ImcBINOP.Oper.EQU;
import static compiler.data.imcode.ImcBINOP.Oper.GEQ;
import static compiler.data.imcode.ImcBINOP.Oper.GTH;
import static compiler.data.imcode.ImcBINOP.Oper.IOR;
import static compiler.data.imcode.ImcBINOP.Oper.LEQ;
import static compiler.data.imcode.ImcBINOP.Oper.LTH;
import static compiler.data.imcode.ImcBINOP.Oper.MOD;
import static compiler.data.imcode.ImcBINOP.Oper.MUL;
import static compiler.data.imcode.ImcBINOP.Oper.NEQ;
import static compiler.data.imcode.ImcBINOP.Oper.SUB;
import static compiler.data.imcode.ImcBINOP.Oper.XOR;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.Vector;

import compiler.common.report.Report;
import compiler.data.abstree.AbsArrExpr;
import compiler.data.abstree.AbsAssignStmt;
import compiler.data.abstree.AbsAtomExpr;
import compiler.data.abstree.AbsBinExpr;
import compiler.data.abstree.AbsBlockExpr;
import compiler.data.abstree.AbsCastExpr;
import compiler.data.abstree.AbsDecl;
import compiler.data.abstree.AbsDelExpr;
import compiler.data.abstree.AbsExpr;
import compiler.data.abstree.AbsExprStmt;
import compiler.data.abstree.AbsFunDecl;
import compiler.data.abstree.AbsFunDef;
import compiler.data.abstree.AbsFunName;
import compiler.data.abstree.AbsIfStmt;
import compiler.data.abstree.AbsNewExpr;
import compiler.data.abstree.AbsRecExpr;
import compiler.data.abstree.AbsSource;
import compiler.data.abstree.AbsStmt;
import compiler.data.abstree.AbsStmts;
import compiler.data.abstree.AbsUnExpr;
import compiler.data.abstree.AbsVarDecl;
import compiler.data.abstree.AbsVarName;
import compiler.data.abstree.AbsWhileStmt;
import compiler.data.abstree.visitor.AbsFullVisitor;
import compiler.data.imcode.ImcBINOP;
import compiler.data.imcode.ImcCALL;
import compiler.data.imcode.ImcCJUMP;
import compiler.data.imcode.ImcCONST;
import compiler.data.imcode.ImcESTMT;
import compiler.data.imcode.ImcExpr;
import compiler.data.imcode.ImcJUMP;
import compiler.data.imcode.ImcLABEL;
import compiler.data.imcode.ImcMEM;
import compiler.data.imcode.ImcMOVE;
import compiler.data.imcode.ImcNAME;
import compiler.data.imcode.ImcSEXPR;
import compiler.data.imcode.ImcSTMTS;
import compiler.data.imcode.ImcStmt;
import compiler.data.imcode.ImcTEMP;
import compiler.data.imcode.ImcUNOP;
import compiler.data.layout.AbsAccess;
import compiler.data.layout.Access;
import compiler.data.layout.Frame;
import compiler.data.layout.Label;
import compiler.data.layout.RelAccess;
import compiler.data.type.SemRecType;
import compiler.data.type.SemType;
import compiler.phases.frames.Frames;
import compiler.phases.seman.SemAn;
import compiler.phases.seman.TypeResolver;

/**
 * Intermediate code generator.
 * 
 * This is a plain full visitor
 * 
 * @author sliva
 */
public class CodeGenerator extends AbsFullVisitor<Void, Stack<Frame>> {

    void put(AbsStmt absStmt, ImcStmt imcStmt) {ImcGen.stmtImCode.put(absStmt, imcStmt);}
    void put(AbsStmts absStmts, ImcStmt imcStmt) {ImcGen.stmtsImCode.put(absStmts, imcStmt);}
    void put(AbsExpr absExpr, ImcExpr imcExpr) {ImcGen.exprImCode.put(absExpr, imcExpr);}

    ImcStmt get(AbsStmt absStmt) {return ImcGen.stmtImCode.getExisting(absStmt);}
    ImcStmt get(AbsStmts absStmts) {return ImcGen.stmtsImCode.getExisting(absStmts);}
    ImcExpr get(AbsExpr absExpr) {return ImcGen.exprImCode.getExisting(absExpr);}

    private final Map<AbsBinExpr.Oper, ImcBINOP.Oper> binopAbsImc = new HashMap<>() {{
        put(AbsBinExpr.Oper.ADD, ADD);
        put(AbsBinExpr.Oper.SUB, SUB);
        put(AbsBinExpr.Oper.MUL, MUL);
        put(AbsBinExpr.Oper.DIV, DIV);
        put(AbsBinExpr.Oper.MOD, MOD);
        put(AbsBinExpr.Oper.EQU, EQU);
        put(AbsBinExpr.Oper.NEQ, NEQ);
        put(AbsBinExpr.Oper.LEQ, LEQ);
        put(AbsBinExpr.Oper.GEQ, GEQ);
        put(AbsBinExpr.Oper.GTH, GTH);
        put(AbsBinExpr.Oper.LTH, LTH);
        put(AbsBinExpr.Oper.IOR, IOR);
        put(AbsBinExpr.Oper.XOR, XOR);
        put(AbsBinExpr.Oper.AND, AND);
    }};


	@Override
	public Void visit(AbsArrExpr arrExpr, Stack<Frame> visArg) {
        // Resolve intermediate code for array, index
		arrExpr.array.accept(this, visArg);
        arrExpr.index.accept(this, visArg);
        
        ImcExpr arrADDR = ((ImcMEM) ImcGen.exprImCode.get(arrExpr.array)).addr;
        ImcExpr index   = ImcGen.exprImCode.get(arrExpr.index);

        // MEM( arrADDR ADD ( CONST(arraySize) MUL index ) )
        put(
            arrExpr,

            new ImcMEM(
                new ImcBINOP(
                    ADD,
                    arrADDR,
                    new ImcBINOP(
                        MUL,
                        new ImcCONST(SemAn.ofType.getExisting(arrExpr).size()),
                        index
                    )
                )
            )
        );

		return null;
    }

	@Override
	public Void visit(AbsAssignStmt assignStmt, Stack<Frame> visArg) {
        // Resolve intermediate code for lvalue, rvalue
		assignStmt.dst.accept(this, visArg);
        assignStmt.src.accept(this, visArg);
        
        // MOVE( dst , src )
        put(
            assignStmt,
            new ImcMOVE(
                get(assignStmt.dst),
                get(assignStmt.src)
            )
        );

		return null;
    }

	@Override
	public Void visit(AbsAtomExpr atomExpr, Stack<Frame> visArg) {

        // Provide a corresponding ImcCONST for a literal,
        // the exception being strings, which are converted to
        // pointers to char arrays

        ImcExpr expr;

        switch (atomExpr.type) {
            case PTR: case VOID:
                expr = new ImcCONST(0);
                break;
            case BOOL:
                expr = new ImcCONST(atomExpr.expr.equals("true") ? 1 : 0);
                break;
            case INT:
                expr = new ImcCONST(Integer.parseInt(atomExpr.expr));
                break;
            case CHAR:
                expr = new ImcCONST((long)atomExpr.expr.charAt(1));
                break;
            case STR:
                expr = new ImcNAME(Frames.strings.get(atomExpr).label);
                break;
            default: throw new Report.InternalError();
        }

        put(atomExpr, expr);

		return null;
    }

	@Override
	public Void visit(AbsBinExpr binExpr, Stack<Frame> visArg) {
        // Resolve intermediate code for operands
		binExpr.fstExpr.accept(this, visArg);
        binExpr.sndExpr.accept(this, visArg);
        
        // op1 operand op2
        put(
            binExpr,
            new ImcBINOP(
                binopAbsImc.get(binExpr.oper),

                get(binExpr.fstExpr),
                get(binExpr.sndExpr)
            )
        );

		return null;
    }

	@Override
	public Void visit(AbsBlockExpr blockExpr, Stack<Frame> visArg) {
		blockExpr.decls.accept(this, visArg);
		blockExpr.stmts.accept(this, visArg);
        blockExpr.expr.accept(this, visArg);
        
        // Combine block statements and return value expression
        put(
            blockExpr,
            new ImcSEXPR(get(blockExpr.stmts), get(blockExpr.expr))
        );

		return null;
    }

	@Override
	public Void visit(AbsCastExpr castExpr, Stack<Frame> visArg) {
		castExpr.type.accept(this, visArg);
        castExpr.expr.accept(this, visArg);
        
        // We discard casts at this stage.
        //
        // One might do some additional type conversions, but given that all types
        // allocate the same space, it is mostly in the hands of the programmer
        // to do additional conversions.
        //
        //
        // One thing that might be reasonable is mod 256 in the case of casting to char.
        // But this is mostly a gimmick that will be implemented only in the case of
        // code incompatibility issues.
        //
        put(castExpr, get(castExpr.expr));

		return null;
    }

	@Override
	public Void visit(AbsDelExpr delExpr, Stack<Frame> visArg) {
        delExpr.expr.accept(this, visArg);
        
        // "del" is a language primitive that gets replaced with a
        // call to a function that does the actual memory management operation
        put(
            delExpr,
            new ImcCALL(
                new Label("del"),
                new Vector<ImcExpr>() {{ new ImcCONST(0); }}
            )
        );

		return null;
    }

	@Override
	public Void visit(AbsExprStmt exprStmt, Stack<Frame> visArg) {
        exprStmt.expr.accept(this, visArg);
        
        // Statement that does computation, but the result gets
        // discarded away
        put(
            exprStmt,
            new ImcESTMT(get(exprStmt.expr))
        );

		return null;
    }

	@Override
	public Void visit(AbsFunDef funDef, Stack<Frame> visArg) {
        // We push the calling function frame definition on top of
        // the stack (so that it will be available during imc gen)
        visArg.push(Frames.frames.get(funDef));

		funDef.parDecls.accept(this, visArg);
		funDef.type.accept(this, visArg);
        funDef.value.accept(this, visArg);
        
        // Pop frame definition (return to caller)
        visArg.pop();

		return null;
    }

	@Override
	public Void visit(AbsFunName funName, Stack<Frame> visArg) {
        
        AbsFunDecl funDecl = (AbsFunDecl) SemAn.declaredAt.get(funName);
        Frame currFrame = Frames.frames.get(funDecl);

        // If function global, SL will be null
        // If function local, SL will be a reference to a temp present in
        // parent's stack frame
        //
        ImcExpr sl = new ImcCONST(0);

        // Find parent through the stack frame
        // (there might be intermeditate non-parent function calls)
        //
        for (Frame parentFrame : visArg) {
            if (parentFrame.depth == currFrame.depth - 1) {
                sl = new ImcTEMP(parentFrame.FP);
                break;
            }
        }

        // We manually iterate over the arguments
        // funName.args.accept(this, visArg);
        Vector<ImcExpr> args = new Vector<>();

        // Add static link first, so that it will be
        // the last in the stack frame
        args.add(sl);

        
        for(AbsExpr arg : funName.args.args()) {
            arg.accept(this, visArg);
            args.add(get(arg));
        }

        // Function call with imc evaluated arguments
        put(
            funName,
            new ImcCALL(Frames.frames.getExisting(funDecl).label, args)
        );
        
		return null;
    }

	@Override
	public Void visit(AbsIfStmt ifStmt, Stack<Frame> visArg) {
        Label labelTrue = new Label();
        Label labelEnd = new Label();

        ifStmt.cond.accept(this, visArg);
        ImcExpr condition = get(ifStmt.cond);
        ifStmt.thenStmts.accept(this, visArg);
        ImcSTMTS thenStmts = (ImcSTMTS) get(ifStmt.thenStmts);

        Vector<ImcStmt> imcIfVector;

        if (ifStmt.elseStmts.numStmts() == 0) {
            imcIfVector = new Vector<>() {{
                add(new ImcCJUMP(condition, labelTrue, labelEnd));
                add(new ImcLABEL(labelTrue));
                add(thenStmts);
                add(new ImcLABEL(labelEnd));
            }};
        } else {
            Label labelFalse = new Label();
            ifStmt.elseStmts.accept(this, visArg);
            ImcSTMTS elseStmts = (ImcSTMTS) get(ifStmt.elseStmts);

            imcIfVector = new Vector<>() {{
                add(new ImcCJUMP(condition, labelTrue, labelFalse));
                add(new ImcLABEL(labelTrue));
                add(thenStmts);
                add(new ImcJUMP(labelEnd));
                add(new ImcLABEL(labelFalse));
                add(elseStmts);
                add(new ImcLABEL(labelEnd));
            }};
        }
        ImcGen.stmtImCode.put(ifStmt, new ImcSTMTS(imcIfVector));
		return null;
    }

	@Override
	public Void visit(AbsNewExpr newExpr, Stack<Frame> visArg) {
        Vector<ImcExpr> args = new Vector<ImcExpr>();

        // static link because this is function call
        args.add(new ImcCONST(0));
        args.add(new ImcCONST(SemAn.isType.getExisting(newExpr.type).size()));

        put(
            newExpr,
            new ImcCALL(new Label("new"), args)
        );

		return null;
    }

	@Override
	public Void visit(AbsRecExpr recExpr, Stack<Frame> visArg) {

        recExpr.record.accept(this, visArg);
        ImcMEM recordName = (ImcMEM) get(recExpr.record);

        SemType record = SemAn.ofType.getExisting(recExpr.record).actualType();
        AbsDecl type = 
        // get SemType for the accessed member's AbsType object
            // get SymbTable for the record object
            TypeResolver.symbTables.get((SemRecType)record)
                // get member object through the name of the accessed member
                .fnd(recExpr.comp.name)
        ;

        RelAccess recordComponentAcces = (RelAccess) Frames.accesses.getExisting(type);

        put(
            recExpr,
            new ImcMEM(
                new ImcBINOP(
                    ADD,
                    recordName.addr,
                    new ImcCONST(recordComponentAcces.offset)
                )
            )
        );

		return null;
    }

	@Override
	public Void visit(AbsSource source, Stack<Frame> visArg) {
        Stack<Frame> stack = new Stack<Frame>();

		source.decls.accept(this, visArg);
        
        return null;
    }

	@Override
	public Void visit(AbsStmts stmts, Stack<Frame> visArg) {
        Vector<ImcStmt> imcStmts = new Vector<>();

        for (AbsStmt stmt : stmts.stmts()){
            stmt.accept(this, visArg);
            imcStmts.add(ImcGen.stmtImCode.get(stmt));
        }
        put(
            stmts,
            new ImcSTMTS(imcStmts)
        );
		return null;
    }

	@Override
	public Void visit(AbsUnExpr unExpr, Stack<Frame> visArg) {
        unExpr.subExpr.accept(this, visArg);

        ImcExpr subExpr;

        switch (unExpr.oper){
            case ADD: {
                subExpr = get(unExpr.subExpr);
                put(unExpr, subExpr);
                return null;
            }
            case SUB: {
                subExpr = get(unExpr.subExpr);
                put(unExpr, new ImcUNOP(ImcUNOP.Oper.NEG, subExpr));
                return null;
            }
            case NOT: {
                subExpr = get(unExpr.subExpr);
                put(unExpr, new ImcUNOP(ImcUNOP.Oper.NOT, subExpr));
                return null;
            }
            case DATA: {
                subExpr = get(unExpr.subExpr);
                put(unExpr, new ImcMEM(subExpr));
                return null;
            }
            case ADDR: {
                if (SemAn.isAddr.get(unExpr.subExpr)){
                    subExpr = (ImcMEM) get(unExpr.subExpr);
                    put(unExpr, ((ImcMEM)subExpr).addr);
                    return null;
                } else {
                    throw new Report.Error(unExpr, "The expression does not have an address.");
                }
            }

            default: throw new Report.InternalError();
        }
    }

	@Override
	public Void visit(AbsVarName varName, Stack<Frame> visArg) {

        Access varAcces = Frames.accesses.get((AbsVarDecl) SemAn.declaredAt.get(varName));

        if (varAcces instanceof AbsAccess){
            ImcExpr address = new ImcNAME(((AbsAccess) varAcces).label);
            put(varName, new ImcMEM(address));
            return null;
        } else {
            RelAccess access = (RelAccess) varAcces;
            ImcExpr addr = new ImcTEMP(visArg.peek().FP);
            for (int i=access.depth; i<visArg.peek().depth; i++){
                addr = new ImcMEM(addr);
            }
            addr = new ImcBINOP(ImcBINOP.Oper.ADD, addr, new ImcCONST(access.offset));
            put(varName, new ImcMEM(addr));
            return null;
        }
    }

	@Override
	public Void visit(AbsWhileStmt whileStmt, Stack<Frame> visArg) {

        Vector<ImcStmt> imcWhileVector = new Vector<>();
        Label labelStart = new Label();
        Label labelTrue = new Label();
        Label labelEnd = new Label();
        whileStmt.cond.accept(this, visArg);
        ImcExpr condition = ImcGen.exprImCode.get(whileStmt.cond);

        whileStmt.stmts.accept(this, visArg);
        ImcSTMTS stmts = (ImcSTMTS) get(whileStmt.stmts);

        imcWhileVector.add(new ImcLABEL(labelStart));
        imcWhileVector.add(new ImcCJUMP(condition, labelTrue, labelEnd));
        imcWhileVector.add(new ImcLABEL(labelTrue));
        imcWhileVector.add(stmts);
        imcWhileVector.add(new ImcJUMP(labelStart));
        imcWhileVector.add(new ImcLABEL(labelEnd));
        ImcGen.stmtImCode.put(whileStmt, new ImcSTMTS(imcWhileVector));
        return null;
    }

}
