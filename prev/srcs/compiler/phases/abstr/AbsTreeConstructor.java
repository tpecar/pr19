/**
 * @author sliva
 */
package compiler.phases.abstr;

import static compiler.data.dertree.DerNode.Nont.Source;

import compiler.common.report.Report;
import compiler.data.abstree.AbsTree;
import compiler.data.dertree.DerLeaf;
import compiler.data.dertree.DerNode;
import compiler.data.dertree.visitor.DerVisitor;

/**
 * Transforms a derivation tree to an abstract syntax tree.
 * 
 * @author sliva
 */
public class AbsTreeConstructor implements DerVisitor<AbsTree, AbsTree> {

	@Override
	public AbsTree visit(DerLeaf leaf, AbsTree visArg) {
		throw new Report.InternalError();
	}

	@Override
	public AbsTree visit(DerNode node, AbsTree visArg) {

		// We're essentially hijacking this method & completely ignoring the
		// visitor methodology here
		//
		// We only expect to be here at the derivation tree root, where we call
		// the toAST method, which starts calling lambdas defined at the SynAn
		// stage.
		//
		if(node.label != Source)
			throw new Report.InternalError();
		
		// start recursive AST generation at source node
		return node.toAST(null);
	}
}
