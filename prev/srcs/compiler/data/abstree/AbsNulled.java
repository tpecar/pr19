package compiler.data.abstree;

import compiler.common.report.*;
import compiler.data.abstree.visitor.*;

public class AbsNulled extends Location implements AbsTree {

    public AbsNulled(Locatable location) {
        super(location);
    }

    // This node is nulled
    @Override
	public boolean produced() {
		return false;
	}

    // Acceptor for no-op visitor
    //
	@Override
	public <Result, Arg> Result accept(AbsVisitor<Result, Arg> visitor, Arg accArg) {
		return visitor.visit(this, accArg);
	}
}
