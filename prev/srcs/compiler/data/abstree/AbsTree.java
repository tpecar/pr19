/**
 * @author sliva
 */
package compiler.data.abstree;

import compiler.common.report.*;
import compiler.data.abstree.visitor.*;

/**
 * @author sliva
 */
public interface AbsTree extends Locatable {

	public abstract <Result, Arg> Result accept(AbsVisitor<Result, Arg> visitor, Arg accArg);

	// Method to provide indication whether a nullable nonterminal was expanded
	// By default this method will return that it produced.
	// (this is to avoid modifying standard classes)
	//
	public default boolean produced() {
		return true;
	}
}
