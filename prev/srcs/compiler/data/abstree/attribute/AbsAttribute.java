/**
 * @author sliva
 */
package compiler.data.abstree.attribute;

import java.util.*;
import compiler.common.report.*;
import compiler.data.abstree.*;

/**
 * An attribute of the abstract syntax tree node.
 * 
 * @author sliva
 *
 * @param <Node> Nodes that values are associated with.
 * @param <Value> Values associated with nodes.
 */
public class AbsAttribute<Node extends AbsTree, Value> {

	/** Mapping of nodes to values. */
	private HashMap<Node, Value> mapping;

	/** Whether this attribute can no longer be modified or not. */
	private boolean lock;

	/** Description of Node, Value generic parameters */
	private final String attribute, nodeDesc, valueDesc;

	/** Constructs a new attribute. */
	public AbsAttribute() {
		this.attribute	= null;
		this.nodeDesc	= null;
		this.valueDesc	= null;

		mapping = new HashMap<Node, Value>();
		lock = false;
	}

	/** Constructs a new attribute, with additional node, value info. */
	public AbsAttribute(String attribute, String nodeDesc, String valueDesc) {
		this.attribute	= attribute;
		this.nodeDesc	= nodeDesc;
		this.valueDesc	= valueDesc;

		mapping = new HashMap<Node, Value>();
		lock = false;
	}

	/**
	 * Associates a value with the specified abstract syntax tree node.
	 * 
	 * @param node  The specified abstract syntax tree node.
	 * @param value The value.
	 * @return The value.
	 */
	public Value put(Node node, Value value) {
		if (lock)
			throw new Report.InternalError();
		mapping.put(node, value);
		return value;
	}

	/**
	 * Returns a value associated with the specified abstract syntax tree node.
	 * 
	 * @param node The specified abstract syntax tree node.
	 * @return The value.
	 */
	public Value get(Node node) {
		return mapping.get(node);
	}

	/**
	 * Returns a value associated with the specified abstract syntax tree node.
	 * 
	 * @param node The specified abstract syntax tree node.
	 * @return The value.
	 */
	public Value getExisting(Node node) {
		Value value = mapping.get(node);

		if(value == null)
			throw new Report.InternalError
			(
				node,
				attribute != null ?
					String.format("%s attribute: %s has no %s -> %s map",
						attribute, node.getClass().getName(), nodeDesc, valueDesc
					) :
					String.format("%s has no mapping", node.getClass().getName())
			);

		return value;
	}


	/**
	 * Prevents further modification of this attribute.
	 */
	public void lock() {
		lock = true;
	}

}
