/**
 * @author sliva
 */
package compiler.data.symbol;

import java.util.regex.Pattern;

import compiler.common.logger.Loggable;
import compiler.common.logger.Logger;
import compiler.common.report.Locatable;
import compiler.common.report.Location;

/**
 * A symbol recognized by a lexer and passed to the parser.
 * 
 * @author sliva
 */
public class Symbol implements Locatable, Loggable {

	// Common regex subsections
	final static String

	// Identifier starting char
	ID_ST		= "\\p{Alpha}_",
	// Identifier non-starting char
	ID_NONST 	= "\\p{Alnum}_", 

	// End of keyword
	// Keywords should end with a character that can not be part of identifier,
	// otherwise we actually hit an identifier
	KEYW_END = "(?!["+ID_NONST+"])"
	;

	/**
	 * CFG terminals.
	 * 
	 * @author sliva
	 */
	public enum Term {
		EOF				(	"\0"								),

		// Identifier moved to top, so that it is matched first
		// All further matches (keywords, literals) override it
		IDENTIFIER		(	"["+ID_ST+"]+["+ID_NONST+"]*"		),

		// You have to take care to escape all special regex
		// symbols, otherwise you will get zero-length matches
		IOR				(	"\\|"								),
		XOR				(	"\\^"								),
		AND				(	"&"									),
		EQU				(	"=="								),
		NEQ				(	"!="								),
		LTH				(	"<(?![=]+)"							),
		LEQ				(	"<="								),
		GTH				(	">(?![=]+)"							),
		GEQ				(	">="								),
		ADD				(	"\\+"								),
		SUB				(	"-"									),
		MUL				(	"\\*"								),
		DIV				(	"/"									),
		MOD				(	"%"									),
		NOT				(	"!(?![=]+)"							),
		ADDR			(	"\\$"								),
		DATA			(	"@"									),

		// We don't want to match a keyword if it is actually
		// part of an identifier
		NEW				(	"new"					+KEYW_END	),
		DEL				(	"del"					+KEYW_END	),
		ASSIGN			(	"=(?![=]+)"							),
		COLON			(	":"									),
		COMMA			(	","									),
		DOT				(	"\\."								),
		SEMIC			(	";"									),
		LBRACE			(	"\\{"								),
		RBRACE			(	"\\}"								),
		LBRACKET		(	"\\["								),
		RBRACKET		(	"\\]"								),
		LPARENTHESIS	(	"\\("								),
		RPARENTHESIS	(	"\\)"								),
		VOIDCONST		(	"none"					+KEYW_END	),
		BOOLCONST		(	"(true|false)"			+KEYW_END	),
		CHARCONST		(	"'[\\x20-\\x7E]{1}+'"				),
		INTCONST		(	"[0-9]+"							),

		// Any printable (non-whitespace) character except double quotes
		STRCONST		(	"\"[[\\x20-\\x21][\\x23-\\x7E]]*\""	),

		PTRCONST		(	"null"					+KEYW_END	),
		VOID			(	"void"					+KEYW_END	),
		BOOL			(	"bool"					+KEYW_END	),
		CHAR			(	"char"					+KEYW_END	),
		INT				(	"int"					+KEYW_END	),
		PTR				(	"ptr"					+KEYW_END	),
		ARR				(	"arr"					+KEYW_END	),
		REC				(	"rec"					+KEYW_END	),
		DO				(	"do"					+KEYW_END	),
		ELSE			(	"else"					+KEYW_END	),
		END				(	"end"					+KEYW_END	),
		FUN				(	"fun"					+KEYW_END	),
		IF				(	"if"					+KEYW_END	),
		THEN			(	"then"					+KEYW_END	),
		TYP				(	"typ"					+KEYW_END	),
		VAR				(	"var"					+KEYW_END	),
		WHERE			(	"where"					+KEYW_END	),
		WHILE			(	"while"					+KEYW_END	)
		;

		public final Pattern pattern;

		private Term(String pattern) {
			this.pattern = Pattern.compile(pattern);
		}
	}

	/** The token. */
	public final Term token;

	/** The lexeme. */
	public final String lexeme;

	/** The location within a source file. */
	private Location location;

	/**
	 * Constructs a new symbol.
	 * 
	 * @param token    The token.
	 * @param lexeme   The lexeme.
	 * @param location The location within a source file.
	 */
	public Symbol(Term token, String lexeme, Locatable location) {
		this.token = token;
		this.lexeme = lexeme;
		this.location = location.location();
	}

	@Override
	public Location location() {
		return location;
	}

	@Override
	public void log(Logger logger) {
		if (logger == null)
			return;
		logger.begElement("term");
		logger.addAttribute("token", token.toString());
		logger.addAttribute("lexeme", lexeme);
		location.log(logger);
		logger.endElement();
	}

	@Override
	public String toString() {
		return lexeme;
	}

}
