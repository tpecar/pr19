/**
 * @author sliva
 */
package compiler.data.dertree;

import compiler.common.report.Locatable;
import compiler.data.abstree.AbsTree;
import compiler.data.dertree.visitor.DerVisitor;

/**
 * Derivation tree.
 * 
 * @author sliva
 */
public abstract class DerTree implements Locatable {

	/**
	 * The method implementing the acceptor functionality.
	 * 
	 * @param         <Result> The type of result the visitor produces.
	 * @param         <Arg> The optional argument that the visitor can pass around.
	 * @param visitor The accepted visitor.
	 * @param accArg  The acceptor's argument.
	 * @return The acceptor's result.
	 */
	public abstract <Result, Arg> Result accept(DerVisitor<Result, Arg> visitor, Arg accArg);

	/**
	 * Run toAST (nonterminal to AbsTree conversion).
	 * Here to essentially bypass the visitor, which does not make sense at this
	 * stage anyway.
	 * 
	 * @param lsub Left subtree that gets appended as a child of this node.
	 */
	public abstract AbsTree toAST(AbsTree lsub);

	// Method to provide indication whether a nullable nonterminal was expanded
	// By default this method will return that the nonterminal produced.
	// (this is to avoid modifying standard classes)
	//
	public boolean produced() {
		return true;
	}
}
