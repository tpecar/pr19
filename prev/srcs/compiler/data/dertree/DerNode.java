/**
 * @author sliva
 */
package compiler.data.dertree;

import java.util.Vector;
import java.util.function.Function;

import compiler.common.report.Location;
import compiler.common.report.Report;
import compiler.data.abstree.AbsTree;
import compiler.data.dertree.visitor.DerVisitor;

import compiler.data.abstree.AbsNulled;

/**
 * An internal node of the derivation tree.
 * 
 * @author sliva
 */
public class DerNode extends DerTree {

	/**
	 * CFG nonterminals.
	 * 
	 * @author sliva
	 */
	public enum Nont {
		Source,			Decls,			Decl,			ParDeclsEps,	ParDecls,
		BodyEps,		Type,			CompDecls, 		Expr,			DisjExpr,
		DisjExprRest,	ConjExpr,		ConjExprRest,	RelExpr,		RelExprRest,
		AddExpr,		AddExprRest,	MulExpr,		MulExprRest,	PrefExpr,
		PstfExpr,		AtomExpr,		CallEps,		Args,			ArgsRest,
		CastEps,		WhereEps,		Stmts,			Stmt,			AssignEps,
		ElseEps
	};

	/** The CFG nonterminal this node represents. */
	public final Nont label;

	/** A list of subtrees (from left to right, ordered). */
	private final Vector<DerTree> subtrees;

	/**
	 * The DerNode to AbsTree conversion lambda.
	 * 
	 * This gets added at the end of the production and provides the conversion
	 * functionality for the AbsTreeconstructor.
	 * 
	 * Input argument is expected to be the left subtree of the parent node that
	 * gets moved under the AST node under conversion.
	 * 
	 *    This is due to the LL(1) grammar, where the operator is the first
	 *    child of the nonterminal, with the 1. operand being in the
	 *    previously expanded production.
	 * 
	 * The DerNode under conversion is in the enclosing scope of the lamda.
	 */
	protected Function<AbsTree, AbsTree> toAST;

	/** Location of a part of the program represented by this node. */
	private Location location;

	/**
	 * Constructs a new internal node of the derivation tree. Immediately after
	 * construction, the list of subtrees is empty as no subtrees have been appended
	 * yet.
	 * 
	 * @param label The CFG nonterminal this node represents.
	 */
	public DerNode(Nont label) {
		this.label = label;
		this.subtrees = new Vector<DerTree>();
	}

	/**
	 * Add a new subtree to this node. Subtrees are always added from left to right.
	 * 
	 * @param subtree The subtree to be added to this node.
	 * @return This node.
	 */
	public DerNode add(DerTree subtree) {
		subtrees.addElement(subtree);
		Location location = subtree.location();
		this.location = (this.location == null) ? location
				: ((location == null) ? this.location : new Location(this.location, location));
		return this;
	}

	/**
	 * Returns the list of subtrees.
	 * 
	 * @return The list of subtrees.
	 */
	public Vector<DerTree> subtrees() {
		return new Vector<DerTree>(subtrees);
	}

	/**
	 * Returns the specified subtree.
	 * 
	 * @param index The index of the subtree (from left to right).
	 * @return The specified subtree.
	 */
	public DerTree subtree(int index) {
		return subtrees.elementAt(index);
	}

	/**
	 * Returns the number of subtrees of this node.
	 * 
	 * @return The number of subtrees of this node.
	 */
	public int numSubtrees() {
		return subtrees.size();
	}

	@Override
	public Location location() {
		return location;
	}

	@Override
	public <Result, Arg> Result accept(DerVisitor<Result, Arg> visitor, Arg accArg) {
		return visitor.visit(this, accArg);
	}

	@Override
	public AbsTree toAST(AbsTree lsub) {
		// Checks if this node is nulled
		// In this case, the node also has no conversion lambda specified.
		// Skip conversion & return AbsNulled for AST
		if(this.subtrees().isEmpty())
			return new AbsNulled(new Location(0,0, 0,0));
		
		// We should already have a lambda provided before this calls
		// If it is missing, we've forgot to implement a production @ SynAn
		if(this.toAST == null)
			throw new Report.InternalError();

		// Note that you have to call the lambda functionality by its method
		// (.apply() in the case of Function lambda)
		return this.toAST.apply(lsub);
	}

}
