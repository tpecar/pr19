package compiler.data.dertree;

import compiler.common.report.Location;
import compiler.data.abstree.AbsNulled;
import compiler.data.abstree.AbsTree;
import compiler.data.dertree.visitor.DerVisitor;

// Extending DerNode, since terminals can't be nulled
public class DerNulled extends DerNode {
    
    public DerNulled() {
        super(null);
    }

    // Acceptor for no-op visitor
    //
    @Override
	public <Result, Arg> Result accept(DerVisitor<Result, Arg> visitor, Arg accArg) {
		return visitor.visit(this, accArg);
    }

    // Non-produced derivation node will convert to AbsNulled
    @Override
	public AbsTree toAST(AbsTree lsub) {
        return new AbsNulled(new Location(0,0, 0,0));
    }

	public boolean produced() {
		return false;
	}

}