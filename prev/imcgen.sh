clear &&
java -cp ./bin/ compiler.Main --target-phase=imcgen --logged-phase=imcgen --xsl=../../data/ ./prgs/imcgen/$1.prev
chromium --allow-file-access-from-files ./prgs/imcgen/$1.imcgen.xml &>/dev/null &