# Random ideas, bits & bobs
_... that didn't find a better home yet._

## 3rd asignment optimizations
An idea for the 3rd assignment - instead of reconstructing which production we
took, why don't we provide that along with the tag? So instead of just the
nonterminal of the node we also save a Production class, which basically knows
how to convert the current node to AST.

Or even better, do the same way as in the terminals, just do it with the nonterminals -
provide the methods as lambdas along with Nont enums. Do it like a HashMap which for the
first terminal / nonterminal of the production provides a class that has a produce() and toAst()
method. This class gets saved as a tag instead of the nonterminal itself.