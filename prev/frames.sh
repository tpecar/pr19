clear &&
java -cp ./bin/ compiler.Main --target-phase=frames --logged-phase=frames --xsl=../../data/ ./prgs/frames/$1.prev
chromium --allow-file-access-from-files ./prgs/frames/$1.frames.xml &>/dev/null &