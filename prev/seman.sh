clear &&
java -cp ./bin/ compiler.Main --target-phase=seman --logged-phase=seman --xsl=../../data/ ./prgs/seman/$1.prev
chromium --allow-file-access-from-files ./prgs/seman/$1.seman.xml &>/dev/null &