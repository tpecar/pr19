# Prevajalniki 2019
_Prevajalniške Javanture z vašim stalnim gostiteljem, TP!_

## Kako se bomo lotili tega zdaj

Ideja letosnjega leta je, da ne pretiravamo s kompleksnostjo.

1. Prednost naj ima razumljivost ter prilagodljivost kode.
2. Opiraj se cim vec na standardno javansko knjiznico, kar pomeni njene algoritme, podatkovne strukture.
3. Raje razsiri razrede std. knjiznice, uporabi generike, kot da odkrivas toplo vodo sam.
4. Poskusaj uporabljati lamde - a do neke sane mere.

In se se bo naslo tekom razvoja.

Naj ti bo v glavnem cilj, da ko prides do koncnega izpita, da bo prevajalnik mozno v celoti razstaviti ter preoblikovati v 15 min, pa bo delal prav, ceprav pocasi.
